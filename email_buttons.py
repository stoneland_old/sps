import sps


s = sps.Scraper()
s.get_opportunities()

ops = s.opportunities
total = str(len(ops))
for x in ops:
    print ("creating button for id: {0} ({1} of {2})".format(x["id"], str(ops.index(x)+1), total))
    o = sps.Opportunity(x["id"])
    o.create_quick_email_button()
