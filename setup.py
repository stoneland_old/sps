from setuptools import setup, find_packages

setup(
    name='sps',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'beautifulsoup4>=4.6.0, <5.0',
        'deepdiff>=3.3.0, <4.0.0',
        'GitPython>=2.1.11',
        'lxml>=3.8, <4.3',
        'peewee>3.8, <4.0',
        'python-dotenv>=0.9.0',
        'requests==2.18.2, <3.0',
        'sparkpost>=1.3.5, <2.0',
        'pandas>=0.24.0',
        'feedparser>=5.2.1',
        'Click>=7.0',
        'gspread==3.1.0',
        'pytz>=2018.9',
        'Pillow>=6.0.0',
        'pytesseract>=0.2.6',
        'python-dateutil>=2.7.5',
        'Unidecode>=1.0.23',
        'opencv-python-headless>=4.0.0.21', 
        'timezonefinder>=4.0.2',
        'SQLAlchemy==1.3.3'
    ],
    entry_points='''
        [console_scripts]
        sps=sps.cli:main
    '''
)
