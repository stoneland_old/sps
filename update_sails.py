import sps

s = sps.Scraper()

wks = sps.MasterControlPlan()

for p in wks.pos_to_update:
    poid = s.get_po_id(p)
    po = sps.PO(poid, session=s.session)
    po.update_from_mcp(wks)
