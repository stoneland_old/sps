#!/usr/bin/env python

# Standard Library Imports
import datetime

# Third-party imports
from peewee import *

# Local Imports
from .config import Config


db = SqliteDatabase(Config.DB_NAME, pragmas={'journal_mode':'wal', 'foreign_keys':1, 'cache_size': -1 * 64000})

class Item(Model):
    id = IntegerField(primary_key=True)
    name = CharField()
    category = CharField()
    color = CharField()
    finish = CharField()
    image = CharField()
    thickness_cm = CharField()
    last_updated = DateTimeField()
    
    class Meta:
        database = db
        table_name = "item"
    

class Bundle(Model):
    id = IntegerField(primary_key=True)
    item = ForeignKeyField(Item, backref="bundles")
    height_in = CharField(null=True)
    length_in = CharField(null=True)
    total_slabs = CharField(null=True)
    total_sqft = CharField(null=True)
    single_slab_sqft = CharField(null=True)
    image = CharField(null=True)
    last_updated = DateTimeField()
    location = CharField(null=True)

    class Meta:
        database = db
        table_name = "bundle"


def create():
    with db:
        db.create_tables([Item, Bundle])

def drop():
    with db:
        db.drop_tables([Item, Bundle])
