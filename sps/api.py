#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

# Standard library imports
import copy
import datetime
import html
import json
import logging
import os
import pytz
import random
import re
import requests
import shutil
import subprocess
import sys
import time
import urllib

# Third party imports
from bs4 import BeautifulSoup as bs
from deepdiff import DeepDiff
import feedparser
import git
import pandas as pd
import peewee
import pendulum

from oauth2client.service_account import ServiceAccountCredentials
from sparkpost import SparkPost

# Local Imports
from .config import Config
from .gsuite import MasterControlPlan
from .urls import URLS
from .tt import MSCContainer, HapagLloydContainer, APLContainer, CMACGMContainer, AliancaContainer
from .outlook import Outlook
from . import utilities
from . import models



# ============
# Main Classes
# ============



class Scraper(object):

    """
    A stoneprofits.com object being able to access all data that is typically accessible on a browser
    """

    # Create a requests session and signin to stoneprofits.com
    def __init__(self):
        
        self.logger = logging.getLogger(__name__)
        self.logger.warning("This is working!")
        
        self.signed_in = False
        self.pos = [] 
        self.sales_orders = []
        self.dd = ''
        headers = {
            "Host": URLS.util_raw, 
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36", 
            "Origin": URLS.util_signin, 
            "Upgrade-Insecure-Requests": "1", 
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Referer": URLS.util_signin, 
            "Accept-Encoding":"gzip, deflate, br", 
            "Accept-Language":"en-US,en;q=0.9"
          }

        self.session = requests.session()
        self.session.headers.update(headers)
        self.signin()
        self.get_pos()


    def signin(self):
        self.r = self.session.get(URLS.util_signin)
        self.logger.info("navigated to {0}".format(URLS.util_signin))

        self.soup = bs(self.r.content, "lxml")

        viewstate = self.soup.find("input", {"id":"__VIEWSTATE"})['value']
        viewstategen = self.soup.find("input", {"id":"__VIEWSTATEGENERATOR"})['value']
        eventvalidation = self.soup.find("input", {"id":"__EVENTVALIDATION"})['value']
        
        postdata = {"__VIEWSTATE": viewstate, 
                    "__VIEWSTATEGENERATOR": viewstategen,
                    "__EVENTVALIDATION": eventvalidation,
                    "cLogin$dbUsername":Config.SPS_USERNAME, 
                    "cLogin$dbPassword":Config.SPS_PASSWORD, 
                    "dbLocalTime":"480", 
                    "x":"0", 
                    "y":"0"}

        temp = self.session.post(URLS.util_signin, data=postdata)
        self.r = self.session.get(URLS.portal_home)
        self.url = self.r.url

        #check if signed in
        self.soup = bs(self.r.content, "lxml")
        if self.soup.find("td", {"id":"top_DPloginname"}):
            self.signed_in = True
            self.logger.info("signed in")


    def get_inventory(self):
        self.r = self.session.get(URLS.list_inventorysearch)
        rawinv = self.r.json()
        
        self.inv_light = []
        for it in rawinv:
            ni = {}
            for k,v in it.items():
                k=k.lower()
                try:
                    v=v.lower()
                except AttributeError:
                    v=v
                ni[k]=v
            self.inv_light.append(ni)

    def get_item_ids(self):
        all_links = []
        self.r = self.session.get(URLS.list_items.format("1"))
        self.soup = bs(self.r.content, "lxml")
        al = self.soup.find_all("a")
        for a in al:
            try:
                href = a.attrs["href"]
                if href.startswith("vItem.aspx?ID="):
                    all_links.append(href)
            except KeyError:
                pass

        tp = self.soup.find(id="TotalPages_listItems").text
        
        try:
            total_pages = int(tp)
        except ValueError:
            total_pages = 1

        for page in range(2, total_pages+1):
            
            self.r = self.session.get(URLS.list_items.format(str(page)))
            self.soup = bs(self.r.content, "lxml")
            al = self.soup.find_all("a")
            for a in al:
                try:
                    href = a.attrs["href"]
                    if href.startswith("vItem.aspx?ID="):
                        all_links.append(href)
                except KeyError:
                    pass
            
            
        all_links = list(set(all_links))
        self.item_ids = []
        for l in all_links:
            i = int(l.split("=")[-1])
            self.item_ids.append(i)

        self.item_ids.sort()    
       
    
    def landed_cost_report(self, beg_date, end_date):
        if isinstance(beg_date, datetime.datetime) and isinstance(end_date, datetime.datetime):
            bds = datetime.datetime.strftime(beg_date, "%Y-%m-%d")
            eds = datetime.datetime.strftime(end_date, "%Y-%m-%d")

            r = self.session.get(URLS.report_purchase_analysis.format(bds, eds, 1))
            soup = bs(r.content, "lxml")
            
            tp = soup.find(id="TotalPages_listReport") 
            try:
                total_pages = int(tp.text)
            except ValueError:
                total_pages = 1

            table = soup.find(id="listReportTable")
            df = pd.read_html(str(table)) 
            df = df[0].iloc[:-1]
            df.columns = ["sipl", "received_date", "name", "sku", "category", "type", \
                            "supplier", "received_qty", "units", "received_slabs", \
                            "received_value","sold_qty", "sold_slabs", "sold_pct", \
                            "instock_qty", "instock_slabs", "instock_value"]
            return df

            
    def get_customer_ids(self):
        all_links = []
        r = self.session.get(URLS.list_customers.format("1"))
        soup = bs(r.content, "lxml")
        tp = soup.find(id="TotalPages_listReport")
        
        try:
            total_pages = int(tp.text)
        except ValueError:
            total_pages = 1
        
        al = soup.find_all("a")
        
       
        for a in al:
            try:
                href = a.attrs["href"]
                if href.startswith("vCustomer.aspx?ID="):
                    all_links.append(a.attrs["href"])
            except KeyError:
                pass
                
        for page in range(2, total_pages+1):
            
            r = self.session.get(URLS.list_customers.format(str(page)))
            soup = bs(r.content, "lxml")
            al = soup.find_all("a")
            for a in al:
                try:
                    href = a.attrs["href"]
                    if href.startswith("vCustomer.aspx?ID="):
                        all_links.append(href)
                except KeyError:
                    pass
 
        all_links = list(set(all_links))

        self.customer_ids = []
        for l in all_links:
            i = int(l.split("=")[-1])
            self.customer_ids.append(i)


        self.customer_ids.sort()
        
    def get_sales_orders(self):
        so_reg = re.compile("^\d{6,7}")
        r = self.session.get(URLS.list_salesorders.format(1))
        soup = bs(r.content, "lxml")
        aaa = soup.find_all("a")
        
        tp = soup.find(id="TotalPages_listSaleOrders")
        
        try:
            total_pages = int(tp.text)
        except ValueError:
            total_pages = 1
        except AttributeError:
            print ("something went wrong")
            total_pages = 1
       
     
        for a in aaa:
            d = {}
            try:
                if a.attrs["href"].startswith("vSaleOrder.aspx?ID="):
                    found = so_reg.match(a.text)
                    if found:
                        d["number"] = a.text.strip()
                        d["id"] = a.attrs["href"].split("=")[-1]
                        self.sales_orders.append(d)
            except KeyError:
                pass
       

         
        #for page in range(2, total_pages+1): too many pages (150+) to go through all of them
        for page in range(2, 7):
            
            r = self.session.get(URLS.list_salesorders.format(page))
            soup = bs(r.content, "lxml")
            aaa = soup.find_all("a")
            for a in aaa:
                d = {}
                try:
                    if a.attrs["href"].startswith("vSaleOrder.aspx?ID="):
                        found = so_reg.match(a.text)
                        if found:
                            d["number"] = a.text.strip()
                            d["id"] = a.attrs["href"].split("=")[-1]
                            self.sales_orders.append(d)
                except KeyError:
                    pass

    def get_pos(self):
        
        po_reg = re.compile("^\d{6,7}")
        
        r = self.session.get(URLS.list_po.format(1))
        soup = bs(r.content, "lxml")

        tp = soup.find("span", {"id": "TotalPages_ListPurchaseOrders"})
        try:
            total_pages = int(tp.text)
        except ValueError:
            total_pages = 1

        # get POs from first page
        links = soup.find_all("a")
        for l in links:
            f = po_reg.search(l.text)
            if f:
                po_number = f.group(0)
                po_url = URLS.util_base+l.attrs['href']
                po_id = po_url.split("=")[-1]
                self.pos.append({"id": po_id, "number": po_number, "url": po_url})
 
        # get POs from page 2 through page 6. COULD use total_pages (instead of '6') for this as well.  
        for x in range(2, 6): 
            r = self.session.get(URLS.list_po.format(x))
            soup = bs(r.content, "lxml")
            links =soup.find_all("a") 
            for l in links:
                f = po_reg.search(l.text)
                if f:
                    po_number = f.group(0)
                    po_url = URLS.util_base+l.attrs['href']
                    po_id = po_url.split("=")[-1]
                    self.pos.append({"id": po_id, "number": po_number, "url": po_url})
        
        
        
    def get_po_id(self, po_number):
        poid = ""
        if self.pos:
            for p in self.pos:
                if p["number"] == str(po_number):
                    poid = p["id"]   
        return poid
    
    def get_opportunities(self):
        self.opportunities = []
        op_reg = re.compile("^\d+")
        r = self.session.get(URLS.list_opportunities.format(1))
        soup = bs(r.content, "lxml")
        links = soup.find_all("a")

        tp = soup.find("span", {"id": "TotalPages_ListOpportunities"})
        try:
            total_pages = int(tp.text)
        except ValueError:
            total_pages = 1

        for l in links:
            f = op_reg.search(l.text)
            if f:
                try:
                    if l.attrs['href'].startswith("vOpportunity.aspx?ID="):
                        op_num = l.text.strip()
                        op_url = URLS.util_base + l.attrs["href"]
                        op_id = l.attrs["href"].split("=")[-1].strip()
                        self.opportunities.append({"number": op_num, "id": op_id, "url": op_url})
                except KeyError:
                    pass

        for x in range(2, 6): 
            r = self.session.get(URLS.list_opportunities.format(x))
            soup = bs(r.content, "lxml")
            links = soup.find_all("a") 
            for l in links:
                f = op_reg.search(l.text)
                if f:
                    try:
                        if l.attrs['href'].startswith("vOpportunity.aspx?ID="):
                            op_num = l.text.strip()
                            op_url = URLS.util_base + l.attrs["href"]
                            op_id = l.attrs["href"].split("=")[-1].strip()
                            self.opportunities.append({"id": op_id, "number": op_num, "url": op_url})
                    except KeyError:
                        pass


    def inventory_changed(self, oldfile="oldinv.json"):
        """
        Compares current inventory to old inventory (stored in oldinv.json). 
        This is a quick check (~3-5 seconds) to see if the entire inventory is worth scraping.
        Returns True if anything changed on this page: https://url_prefix.stoneprofits.com/listInventorySearch.aspx?act=getItems
        """
        # grab current inventory from sps.
        # grab old inventory from file.
        self.get_inventory()
        current_inventory = self.inv_light
        
        if os.path.isfile(oldfile):
            with open(oldfile) as f:
                old_inventory= json.load(f)
        else:
            old_inventory = ""
         
        #update oldjson file
        with open(oldfile, "w") as f:
            json.dump(current_inventory,f)
        
        if old_inventory:       
            self.dd = DeepDiff(old_inventory, current_inventory)
            if self.dd:
                return True
            else:
                return False
        else:
            return False
        
            
    # Logout of SPS
    def logout(self):
        self.r = self.session.get(URLS.util_logout)
        self.soup = bs(self.r.content, "lxml")
        self.logger.info("signed out of sps")
        return self.r.url

    # Save current screen in a file    
    def current_screen(self):
        with open(Config.SPS_SCREENSHOT, "w") as f:
            f.write(self.r.text)
        self.logger.info("saved screenshot of current page to {}".format(Config.SPS_SCREENSHOT))
        return os.path.abspath(Config.SPS_SCREENSHOT)



class Customer(object):
    
    def __init__(self, customer_id, session=None):
        self.id = str(customer_id)
        self.logger = logging.getLogger(__name__)

        self.url = URLS.view_customer.format(self.id)
        self.edit_url = URLS.edit_customer.format(self.id)
        self.post_url = URLS.post_customer.format(self.id)

        if session:
            self.session = session
        else:
            s = Scraper()
            self.session = s.session
        self.edit_template = Config.SPS_CUSTOMER_EDIT_TEMPLATE
        self.find_state()


    
    def find_state(self):
        r = self.session.get(self.edit_url)
        self.edit_soup = bs(r.content, "lxml")

        self.state = copy.copy(self.edit_template)
        for k, v in self.state.items():
            if k == "dbDeliveryType":
                pass
            elif k == "dbContactMode":
                pass
            else:
                x = self.edit_soup.find(id=k)
                if x.name.lower() == "input":
                    if x.attrs["type"] == "checkbox":
                        try:
                            checked = x.attrs["checked"].lower()
                            if checked == "checked":
                                self.state[k] = "on"
                        except KeyError:
                            self.state[k] = ""
                    else: 
                        try:
                            self.state[k] = x.attrs['value']
                        except KeyError:
                            self.state[k] = ""
                elif x.name.lower() == "select":
                    option = x.find("option", {"selected":"selected"})
                    try:
                        self.state[k] = option.attrs["value"].strip()
                    except AttributeError:
                        print (option)
                        self.state[k] = ""
                elif x.name.lower() == "textarea":
                    self.state[k] = x.text

        


        

class PO(object):
    
    def __init__(self, po_id, session=None):
        self.logger = logging.getLogger(__name__)

        self.id = str(po_id)
        
        self.edit_url = URLS.edit_po.format(self.id)
        self.view_url = URLS.detail_po.format(self.id)
        self.post_url = URLS.post_po.format(self.id, "{0}") # second is str(int(time.time()))

        self.edit_template = Config.SPS_PO_EDIT_TEMPLATE

        if session:
            self.session = session
        else:
            s = Scraper()
            self.session = s.session
        
        r = self.session.get(self.view_url)
        self.soup = bs(r.content, "lxml")
        self.find_state()


    def is_open(self, force=False):
        _isopen = ""
        if force:
            r = self.session.get(self.view_url)
            self.soup = bs(r.content, "lxml")
        imgs = self.soup.find_all("img")
        for im in imgs:
            if im.attrs["src"].endswith("image/lBar2/status_LG_3of3.gif"):
                _isopen = False
                break
            else:
                _isopen = True
        self.logger.info("PO {0} is open".format(self.number))
        return _isopen


    def find_state(self):
        r = self.session.get(self.edit_url)
        self.edit_soup = bs(r.content, "lxml")
       
        self.state = copy.copy(self.edit_template)
        for k, v in self.state.items():
            x = self.edit_soup.find("input", {"id": k})
            if not x:
                y = self.edit_soup.find("select", {"id": k})
                if not y:
                    z = self.edit_soup.find("textarea", {"id": k})
                    if z:
                        self.state[k] = z.text
                else:
                    option = y.find("option", {"selected":"selected"})
                    value = option.attrs['value'].strip()
                    self.state[k] = value
            else:
                try:
                    self.state[k] = x.attrs['value']
                except KeyError:
                    #print ("no value: ", k)
                    self.state[k] = ""
    
    
    # modify property.setters to allow for a second boolean value to disable pushing changes.
    # in other words, just change state locally, don't push to SPS.
    # useful for batching changes instead of making a call per change.
    def batch(self, value, value_type):
        
        push = ""
        newvalue = ""

        if isinstance(value, tuple):
            if len(value) == 2:
                if isinstance(value[0], value_type):
                    if isinstance(value[1], bool):
                        push = value[1]
                        newvalue = value[0]
                    else:
                        raise Exception("second value in tuple needs to be of type '<class bool>'")
                elif not value[0]:
                    pass
                else:
                    raise Exception("value needs to be of type {}".format(value_type))
            else:
                raise Exception("tuple needs to have 2 items -- value and True/False for pushing changes")
        elif isinstance(value, value_type):
            push = True     # assume push is True by default
            newvalue = value
        else:
            raise Exception("value needs to be tuple or of type {}".format(value_type))

        return newvalue, push


    @property
    def eta_port(self):
        d = self.state["dbPortETADate"]
        if d:
            self._eta_port = datetime.datetime.strptime(d, "%m/%d/%Y")
        else:
            self._eta_port = "" 

        return self._eta_port


    @eta_port.setter
    def eta_port(self, value):
        # value must be a datetime object or a tuple
        # with the first value as a datetime
        # second value is True or False.
        # to specify if you want to push the changes. 

        value, push = self.batch(value, datetime.datetime)

        if value:
            str_value = datetime.datetime.strftime(value, "%m/%d/%Y")
            self.state["dbPortETADate"] = str_value
        else:
            self.state["dbPortETADate"] = ""
        
        if push:
            self.change(self.state)


    @property
    def eta_final(self):
        d = self.state["dbInventoryETADate"]
        if d:
            self._eta_final = datetime.datetime.strptime(d, "%m/%d/%Y")
        else:
            self._eta_final = ""

        return self._eta_final


    @eta_final.setter
    def eta_final(self, value):
        # value must be a datetime object or a tuple
        # with the first value as a datetime
        # second value is True or False.
        # to specify if you want to push the changes. 
 
        value, push = self.batch(value, datetime.datetime)
        
        if value:
            str_value = datetime.datetime.strftime(value, "%m/%d/%Y")
            self.state["dbInventoryETADate"] = str_value
        else:
            self.state["dbInventoryETADate"] = ""
      
        # to allow batch changes
        if push: 
            self.change(self.state)

 
     
    @property
    def proforma(self):
        self._proforma = self.state["dbPartyPOorSO"]
        return self._proforma

    
    @proforma.setter
    def proforma(self, value):

        value, push = self.batch(value, str)
        self.state["dbPartyPOorSO"] = value
        
        if push:
            self.change(self.state)


    @property
    def number(self):
        self._number = self.state["dbTransactionNumber"]
        return self._number
    
    @number.setter
    def number(self, value):
        value, push = self.batch(value, str)
        self.state["dbTransactionNumber"] = value
        if push:
            self.change(self.state)
        

    @property
    def vessel(self):
        self._vessel = self.state["dbVessel"]
        return self._vessel

       
    @vessel.setter
    def vessel(self, value):
        value, push = self.batch(value, str)
        self.state["dbVessel"] = value
        if push:
            self.change(self.state)
    

    @property
    def etd(self):
        d = self.state["dbETDPort"]
        if d:
            self._etd = datetime.datetime.strptime(d, "%m/%d/%Y")
        else:
            self._etd = ""
        return self._etd

    
    @etd.setter
    def etd(self, value):
        # value must be a datetime object
        # e.g value = datetime.datetime(2019, 6, 21, 0, 0)
        value, push = self.batch(value, datetime.datetime)
        if value:
            str_value = datetime.datetime.strftime(value, "%m/%d/%Y")
            self.state["dbETDPort"] = str_value
        else:
            self.state["dbETDPort"] = ""
        if push:
            self.change(self.state)
 
    @property
    def container(self):
        c = self.state["dbContainerNumber"]
        self._container = c.replace("/", "").replace(" ", "")
        return self._container

    @container.setter
    def container(self, value):
        value, push = self.batch(value, str)
        value = value.replace("/", "").replace(" ", "")
        self.state["dbContainerNumber"] = value
        if push:
            self.change(self.state)
        
    
    @property
    def date(self):
        d = self.state["dbTransactionDate"]
        if d:
            self._date = datetime.datetime.strptime(d, "%m/%d/%Y")
        else:
            self._date = ""
        return self._date
    
    @date.setter
    def date(self, value):
        # value must be a datetime object
        # e.g value = datetime.datetime(2019, 6, 21, 0, 0)
        value, push = self.batch(value, datetime.datetime)
        if value:
            str_value = datetime.datetime.strftime(value, "%m/%d/%Y")
            self.state["dbTransactionDate"] = str_value
        else:
            self.state["dbTransactionDate"] = ""
        
        if push:
            self.change(self.state)

    @property
    def supplier(self):
        self._supplier_id = self.state["dbAccountingPartyID_ID"]
        
        self._supplier = self.state["dbAccountingPartyID"]
        return {"id": self._supplier_id, "name": self._supplier}
    
    @supplier.setter
    def supplier(self, value):
        value, push = self.batch(value, str)
        raw_sup = self._search_suppliers(value)
        sup_id = raw_sup["dbID"]
        supplier = Supplier(sup_id)
        sup_dict = supplier.change_in_po() 
        self.state.update(sup_dict)
        if push:
            self.change(self.state)
   
 
    @property
    def status(self):
        self._status = self.state["dbTransactionStatus"]
        return self._status
        
    
    @status.setter
    def status(self, value):
        value, push = self.batch(value, str)
        all_statuses = self._find_all_statuses()
        if value in all_statuses:
            self.state["dbTransactionStatus"] = value
            if push:
                self.change(self.state)
        else:
            raise Exception("Status needs to be one of: {0}".format(all_statuses))

            
  
    def change(self, values_dict): 
        self.form_data = copy.copy(self.state)
        for k, v in values_dict.items():
            self.form_data[k] = v

        p = self.session.post(self.post_url.format(str(int(time.time()))), data=self.form_data)
        self.find_state()
        return p


    def close(self):
        r =self.session.post(URLS.close_po+self.id)
        self.find_state()
        return r
    
    def open(self):
        r = self.session.post(URLS.open_po+self.id)
        self.find_state()
        return r

    def update_from_mcp(self, master_control_plan):
        wks = master_control_plan
        if self.is_open():
            print ("syncing PO {}".format(self.number))
            sail = wks.get_po_sail(self.number)

            etd = wks.format_date(sail["ETD"])
            eta_port = wks.format_date(sail["ETA Port"])
            eta_final = wks.format_date(sail["ETA Final"])
            shipping_line = sail["CARRIER"]

            self.vessel = (sail["VESSEL"], False)
            self.etd = (etd, False)
            self.eta_port = (eta_port, False)
            self.eta_final = (eta_final, False)
            
            self.container = (sail["CONTAINER"], False)
           
            heading = "<h4>Container Tracking</h4><h5><i>({0} Line)</i></h5><h5>Last Updated: {1}</h5><br>"
            button = "<a href='{0}' target='_blank'><button><strong>TRACK & TRACE</strong></button></a><br>"
            html = "<div style='text-align:center;'>{h}{b}<br></div>{t}" 

            if self.container: 
               
                if "hapag" in shipping_line.lower():
                    self.sc = HapagLloydContainer(self.container)
                elif "msc" in shipping_line.lower():
                    self.sc = MSCContainer(self.container)
                elif "alian" in shipping_line.lower():
                    self.sc = AliancaContainer(self.container)
                elif "apl" in shipping_line.lower():
                    self.sc = APLContainer(self.container)
                elif "cma" in shipping_line.lower():
                    self.sc = CMAContainer(self.container) 
               
                heading = heading.format(self.sc.shipping_line, self.sc.last_updated_string)
                button = button.format(self.sc.tracking_url)
                html = html.format(h=heading, b=button, t=self.sc.df.to_html(index=False))

                self.state["dbInternalNotes"] = html

            
            today = datetime.datetime.combine(datetime.date.today(), datetime.time())
            if today and eta_final and etd and eta_port: 
                # past ETA date
                if today >= eta_final:
                    self.status = ("Reached Ramp", False)
                # before arrival date but after reaching port
                elif today < eta_final and today >= eta_port:
                    self.status = ("Reached Port", False)
                # after departure but before reaching anywhere
                elif today < eta_final and today < eta_port and today >= etd:
                    self.status = ("Sailed", False)
                # before all dates but there's a container number or booking reference number. 
                elif today < eta_final and today < eta_port and today < etd and (sail["CONTAINER"] or sail["BOOKING"]):
                    self.status = ("Booking Complete", False) 
            self.change(self.state) 
            

    def _find_all_statuses(self):
        all_statuses = []
        raw_opts = self.edit_soup.find("select", {"id":"dbTransactionStatus"}) 
        if raw_opts:
            options = raw_opts.find_all("option")
            for opt in options:
                all_statuses.append(opt.attrs["value"])
            all_statuses = list(filter(None, all_statuses))
            return all_statuses
        else:
            all_statuses = None
    
        
  
    def _search_suppliers(self, search_value):
        payload = {
            "output": "ajax",
            "list": "listdbAccountingPartyID",
            "q2": "Suppliers",
            "tab": "dbAccountingPartyID",
            "sort":"Name",
            "order": "ASC",
            "searchBy": "Name^alpha",
            "searchOperator": "%3D",
            "dbSearchValue1": "",
            }

        payload["dbSearchValue1"] = search_value
        r = self.session.get(URLS.search_lists, params=payload)
        soup = bs(r.content, "lxml")
        table = soup.find("table", {"id": "listdbAccountingPartyIDTable"})
        rows = table.find_all("tr")
        ins = rows[1].find("input")
        value = json.loads(ins.attrs['value'])
        return value

 
class Supplier(object):
    
    def __init__(self, sup_id, session=None):
        self.id = sup_id
        self.url = URLS.detail_supplier.format(self.id)
        self.edit_url = URLS.edit_supplier.format(self.id)

        if session:
            self.session = session
        else:
            s = Scraper()
            self.session = s.session
        
        #r = self.session.get(self.url)
        #self.soup = bs(r.content, "lxml")
        e = self.session.get(self.edit_url)
        self.edit_soup = bs(e.content, "lxml")
        self.edit_template = Config.SPS_SUPPLIER_EDIT_TEMPLATE
        self.find_state()


    def find_state(self):
        
        self.state = copy.copy(template)
        for k, v in self.state.items():
            x = self.edit_soup.find("input", {"id": k})
            if not x:
                y = self.edit_soup.find("select", {"id": k})
                if not y:
                    z = self.edit_soup.find("textarea", {"id": k})
                    self.state[k] = z.text
                else:
                    option = y.find("option", {"selected":"selected"})
                    value = option.attrs['value'].strip()
                    self.state[k] = value
            else:
                try:
                    self.state[k] = x.attrs['value']
                except KeyError:
                    self.state[k] = ""
    
    def change_in_po(self):
        # return a partial dictionary needed to change supplier using PO.supplier
        keys_needed = ("dbName", 
                        "dbAddress", 
                        "dbAddress2", 
                        "dbCity",
                        "dbState",
                        "dbCountry",
                        "dbZip",
                        "dbPaymentTermsID")
        
        substate = {k: self.state[k] for k in keys_needed}
        
        po_change = {"dbAccountingAddress": substate["dbAddress"],
                     "dbAccountingAddress2": substate["dbAddress2"],
                     "dbAccountingCity": substate["dbCity"],
                     "dbAccountingCountry": substate["dbCountry"],
                     "dbAccountingPartyContactID": "",
                     "dbAccountingPartyContactID_ID": "",
                     "dbAccountingPartyID": substate["dbName"],
                     "dbAccountingPartyID_ID": self.id,
                     "dbAccountingState": substate["dbState"],
                     "dbAccountingZip": substate["dbZip"],
                     "dbPaymentTerms": substate["dbPaymentTermsID"]
                    }
        
        return po_change 
            
        
    @property
    def name(self):
        self._name = self.state["dbName"]
        return self._name

    @property
    def address_street_1(self):
        self._address_street_1 = self.state["dbAddress"]
        return self._address_street_1
    
    @property
    def address_street_2(self):
        self._address_street_2 = self.state["dbAddress2"]
        return self._address_street_2

    @property
    def address_city(self):
        self._address_city = self.state["dbCity"]
        return self._address_city

    @property
    def address_state(self):
        self._address_state = self.state["dbState"]
        return self._address_state
    
    @property
    def address_zip(self):
        self._address_zip = self.state["dbZip"]
        return self._address_zip

    @property
    def address_country(self):
        self._address_country = self.state["dbCountry"]            
        return self._address_country

    @property
    def address(self):
        pass


class SupplierInvoice(object):
    
    def __init__(self, si_id):
        self.id = si_id
        


class Email(object):

    def __init__(self):

        self.sparkpost = Config.MAIL_HOST_PASSWORD
        self.from_email = Config.MAIL_FROM


    def send_inventory_reports(self, recipients_list, emailtype='report'):
         
        try: 
            response = self.sparkpost.transmissions.send(
                recipients=recipients_list,
                html="<p>See attached inventory report.</p><br><br><p>--Automated Email--</p>",
                from_email=self.from_email,
                subject="{}: Inventory - On Hold | In Transit | On PO".format(datetime.date.today().strftime("%Y-%m-%d")),
                attachments=[
                    {
                        "name":"not-in-stock-inventory.csv",
                        "type":"text/plain",
                        "filename":"reports/combined.csv"
                    }
                ],
                track_opens=True
            )

        except IOError:
            self.logger.warning("reports were not found in the folder")

        return response



# no signin required
class NSItem(object):

    def __init__(self, image_wrapper):
        self.wrap = image_wrapper

        # regex patterns
        name_pat = re.compile("(.+)\((.+)\)")
        id_pat = re.compile("GetvPage\((\d+)\)")
        

        # find ID
        try:
            id_elem = self.wrap.find("a", {"onclick":lambda value: value and value.startswith("GetvPage")}).attrs['onclick']
            id_match = id_pat.match(id_elem)
            if id_match is not None:
                self.id = id_match.group(1)
            else:
                self.id = ""
        except AttributeError as aex:
            self.id = ""
            print (aex)


        #find name
        try:
            name_elem = self.wrap.find("a", {"class":"fancybox"}).attrs['title']
            name_match = name_pat.match(name_elem).group(1).strip().lower()
            if name_match is not None:
                self.name = name_match
            else:
                self.name = ""
        except AttributeError as aex:
            self.name = ""
            print (aex)

        #find Image URL
        try:
            url = self.wrap.find("a", {"class":"fancybox"}).attrs['href']
            if "imgnotbig" in url.lower():
                self.image = "http://67.202.78.104/{0}web/image/imgnotBig.png".format(Config.URL_PREFIX)
            else:
                self.image = url
        except AttributeError as aex:
            print (aex)
            self.image = ""

        # Find meta info
        try:
            minfo = self.wrap.find("a", {"class":"fancybox"}).attrs['title']
            meta_match = name_pat.match(minfo).group(2).strip().lower()
            if meta_match is not None:
                self.category = meta_match.split("|")[0].strip()
                self.finish = meta_match.split("|")[1].strip()
                self.thickness_cm = float(meta_match.split("|")[2].strip().replace("cm",""))
            else:
                self.category = ""
                self.finish = ""
                self.thickness_cm = ""
        except AttributeError as aex:
            self.category = ""
            self.finish = ""
            self.thickness_cm = ""

            print (aex)
        
        # Find color

        label = self.wrap.next_sibling.next_sibling.next_sibling.next_sibling
        label_list = label.text.split("|")
        if len(label_list) == 1:
            self.color = ""
        elif len(label_list) == 2:
            self.color = label_list[1].split("|")[-1].lower().strip()


    def __repr__(self):
        return "{0} <{1} | {2} | {3}>".format(self.__class__.__name__, self.id, self.name, self.category)

    def __str__(self):
        return self.name


                 

# no signin required
class NSItemDetail(object):

    def __init__(self, item_id, session=None):

        self.bundles = []
        self.id = item_id
        if session is None:
            self.session = requests.session()
        else:
            self.session = session

        self.url = URLS.rtinv_detail + str(self.id)
        r = self.session.get(self.url)
        self.soup = bs(r.content, "lxml")
        raw_bundles = self.soup.find_all("div", {"class":"col-xs-12 col-sm-6 col-md-4"})

        # regex patterns
        bundle_match = re.compile("Bundle#:\s+(\d+)")
        size_match = re.compile("Avg\s+Size:\s+(.+?)\s+SF")
        quantity_match = re.compile("Qty:\s*(.+?)\s+SF")
        location_match = re.compile("Location:(.+)")


        # find details in each bundle
        for rb in raw_bundles:
            d = {}
            d["item_id"] = ""
            d["image"] = ""
            d["bundle_id"] = ""
            d["slab_sqft"] = ""
            d["length"] = ""
            d["height"] = ""
            d["total_sqft"] = ""
            d["total_slabs"] = ""
            d["location"] = ""
            

            d["item_id"] = self.id
            img = rb.find("img").attrs["src"]
            if "imgnotbig" in img.lower():
                d["image"] = URLS.rtinv_noimage
            else:
                d["image"] = img

            label = rb.find("div", {"class":"LabelMore"})
            spans = label.find_all("span")
            
            bno = bundle_match.search(label.text)
            if bno is not None:
                d["bundle_id"] = bno.group(1)
            
            for span in spans:
                if span.text.startswith("Avg"):
                    raw_dims = size_match.search(span.text)
                    if raw_dims is not None:
                        a = raw_dims.group(1).split("=")[0].split("x")[0].replace('"','').strip()
                        b = raw_dims.group(1).split("=")[0].split("x")[1].replace('"','').strip()
                        slab_sqft = raw_dims.group(1).split("=")[0].split("x")[1].replace('"','').strip()
                        d["slab_sqft"] = slab_sqft
                        try:
                            fa = float(a)
                            fb = float(b)
                            if fa > fb:
                                d["length"] = a
                                d["height"] = b
                            elif fa < fb:
                                d["length"] = b
                                d["height"] = a
                            elif fa == fb:
                                d["length"] = a
                                d["height"] = b
                        except ValueError as ve:
                            print (ve)
                elif span.text.startswith("Qty"):
                    raw_qty = quantity_match.search(span.text)
                    if raw_qty is not None:
                        total_sqft = raw_qty.group(1).split("/")[1].strip()
                        total_slabs = raw_qty.group(1).split("/")[0].split(" ")[0].strip()
                        d["total_sqft"] = total_sqft
                        d["total_slabs"] = total_slabs
                elif span.text.startswith("Location"):
                    raw_loc = location_match.search(span.text)
           
                    if raw_loc is not None:
                        d["location"] = raw_loc.group(1).lower().strip()
       
            self.bundles.append(d)
   

 
    def write_to_db(self):
        
        with models.db.atomic():
            for b in self.bundles:
                if b["bundle_id"]:
                    try:
                        bundle = models.Bundle.insert(id=b["bundle_id"], 
                                item=models.Item.get_by_id(b["item_id"]), 
                                height_in=b["height"], 
                                length_in=b["length"], 
                                total_sqft=b["total_sqft"],
                                single_slab_sqft=b["slab_sqft"],
                                total_slabs=b["total_slabs"], 
                                image=b["image"],
                                location = b["location"],
                                last_updated=datetime.datetime.now()).on_conflict_replace()
                        bundle.execute()
                    except peewee.IntegrityError as e:
                        print (e)



# no signin required
class Inventory(object):

    def __init__(self, wtd=True, force=False, itemsonly=False):
        self.base_url = URLS.rtinv_all
        self.item_url = URLS.rtinv_detail
        self.session = requests.session()
        r = self.session.get(self.base_url)
        self.soup = bs(r.content, "lxml")
        self.items = []
        self.bundles = []
        scraper = Scraper()

        if force and itemsonly:
            self.collect_items(wtd=wtd)
        elif force and not itemsonly:
            self.collect_items(wtd=wtd)
            self.collect_bundles(wtd=wtd)
        elif not force and itemsonly:
            if scraper.inventory_changed():
                self.collect_items(wtd=wtd)
            else:
                print ("inventory up to date already")
        elif not force and not itemsonly:
            if scraper.inventory_changed():
                self.collect_items(wtd=wtd)
                self.collect_bundles(wtd=wtd)
            else:
                print ("inventory up-to-date already")



    def collect_items(self, wtd=True):

        img_wrappers = self.soup.find_all("div", {"class":"ImgWrap"})
        for wrapper in img_wrappers:
            item = {}
            i= NSItem(wrapper)
            item['id'] = i.id
            item['category'] = i.category
            item['finish'] = i.finish
            item['image'] = i.image
            item['name'] = i.name
            item['thickness_cm'] = i.thickness_cm
            item['last_updated'] = datetime.datetime.now()
            item['color'] = i.color
            self.items.append(item)
        if wtd:
            self.write_to_db()

    def collect_bundles(self, wtd=True):
        if self.items:
            print ("collecting bundles")
            for item in self.items:
                #print ("getting item detail for {}".format(item))
                itemdet = NSItemDetail(item_id=item['id'],session=self.session)
                self.bundles.extend(itemdet.bundles)
                if wtd:
                    #print ("writing {0} bundles for {1} to db".format(len(itemdet.bundles), item['name']))
                    itemdet.write_to_db()
    
    def write_to_db(self):

        models.db.connect(reuse_if_open=True)

        try:
            models.Item.insert_many(self.items).on_conflict_replace().execute()
        except Exception as e:
            print (e)


        models.db.close()
        

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, new_path):
        self.new_path = os.path.expanduser(new_path)

    def __enter__(self):
        self.saved_path = os.getcwd()
        os.chdir(self.new_path)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.saved_path)



class Repo(object):

    def __init__(self):
        self.remote_path = Config.GIT_REMOTE
        self.local_path = os.path.abspath(Config.GIT_LOCAL)
        self.repo = self._set_repo()
        self.items = models.Item.select()
        if os.path.isdir(os.path.join(self.repo.working_dir, "content/inventory")):
            self.inventory_dir = os.path.join(self.repo.working_dir, "content/inventory")
        else:
            self.inventory_dir = ""
            print ("no inventory files found")
            sys.exit()
                
    def _set_repo(self):
        try:
            repo = git.Repo(self.local_path)
            update = repo.remotes.origin.pull()
        except git.NoSuchPathError:
            repo = git.Repo.clone_from(self.remote_path, self.local_path)
        return repo

    #commit
    def update(self):
        with cd(self.local_path):
            try:
                subprocess.call('hugo')
                self.repo.git.add("--all")
                self.repo.git.commit('-m', 'update inventory {}'.format(datetime.datetime.now()), author=Config.GIT_EMAIL)
                self.repo.git.push()
            except Exception as e:
                print (e)
                
        

    def prep_inventory_dir(self):
        
        # delete all files in inventory directory
        print ("deleting content folder and files")
        for f in os.listdir(self.inventory_dir):
            filepath = os.path.join(self.inventory_dir, f)
            try:
                if os.path.isfile(filepath):
                    #print ("deleting file {}".format(filepath))
                    os.unlink(filepath)
                elif os.path.isdir(filepath):
                    #print ("deleting folder {}".format(filepath))
                    shutil.rmtree(filepath)
            except Exception as e:
                print (e)

    def delete_public_dir(self):
        print ("deleting 'public' directory")
        public_dir = os.path.join(self.repo.working_dir, "public")
        try:
            if os.path.isdir(public_dir):
                shutil.rmtree(public_dir)
            else:
                pass
        except Exception as e:
            print (e)
    
    def create_files_and_folders(self):
        print ("creating files and folder")
        self.folder_names = []
        for item in self.items:
            fn = (item.name+"-"+item.category+"-"+item.finish+"-"+str(item.thickness_cm)).replace(" ","-")
            self.folder_names.append(fn)
            try:
                itempath = os.path.join(self.inventory_dir, fn)
                os.mkdir(itempath)
                
                md = MDFile(item.id)
                md.write_md(itempath)
                
            except OSError as ose:
                print (ose)  
                print ("Creation of the directory {} failed".format(fn))
            else:  
                #print ("Successfully created the directory {}".format(fn))
                pass

    def write_md_files(self, itemid):
        md = MDFile()
        




class MDFile(object):
    # File Structure
    #
    # | inventory
    # |     | absolute-black-granite
    # |     |          | _index.md
    # |     |          | 8601.md
    # |     |          | 9202.md
    # |     | _index.md


 
    def __init__(self, itemid):   # filetype can be inventory, bundle, color
        self.id = itemid
        self.inventory_file = Config.MD_INVENTORY_FILE
        self.bundle_file = Config.MD_BUNDLE_FILE
        self.item_file = Config.MD_ITEM_FILE


    # writes to current directory
    def write_inventory_md(self, filepath="_index.md"):
        with open(filepath, "w") as f:
            f.write(self.inventory_file)
        

    def write_md(self, filepath="."):
         
        desc_template = "{name} is a {category}"
        
        
        #q = models.Item.select(models.Item, models.Bundle).join(models.Bundle).where(models.Item.id==self.id)
        #item = q[0]
        item = models.Item.get_by_id(self.id)

        # temp variables to make reading/editing easier
        modalname = item.name.replace(" ", "_").lower()
        image = item.image
        #slug = (item.name + " " + item.category).replace(" ", "-").lower()
        origin = ""
        description = desc_template.format(name=item.name.title(), category=item.category)
        stonetype = item.category
        thickness= item.thickness_cm
        finish = item.finish
        title = item.name
        color =  item.color
        
        
        on_consignment = True
        bundles_exist = False
       
        # make sure item has bundles 
        if item.bundles:
            bundles_exist = True
        
        # make sure those bundles have a warehouse location (otherwise it means they're on consignment)
        for b in item.bundles:
            if b.location:
                on_consignment = False
       
     
        if bundles_exist and not on_consignment:
            instock = "true"
        else:
            instock = "false"


        indexmd = os.path.join(filepath, "_index.md")

        # write item _index.md file
        with open(indexmd, "w") as f:
            f.write(self.item_file.format(modalname=modalname, 
                                        image=image, 
                                        origin=origin,
                                        thickness=thickness,
                                        description=description, 
                                        stonetype=stonetype, 
                                        title=title, 
                                        finish=finish,
                                        color=color, 
                                        type="bundle",
                                        instock=instock))

        # write bundles files, if any
        if item.bundles: 
            for b in item.bundles:
                if b.location:

                    bundlemd = os.path.join(filepath, str(b.id)+".md")
                    
                    # temp variables to make reading/editing easier
                    bundle_id = b.id
                    thickness = item.thickness_cm + " " + "cm"
                    finish = item.finish
                    height = b.height_in
                    length = b.length_in
                    totalslabs = b.total_slabs
                    totalsqft = b.total_sqft
                    img= b.image
                    location = b.location
                    
                    with open(bundlemd, "w") as f:
                        f.write(self.bundle_file.format(bundle=bundle_id,
                                                        thickness=thickness,
                                                        finish=finish,
                                                        height=height,
                                                        length=length,
                                                        totalslabs=totalslabs,
                                                        totalsqft=totalsqft,
                                                        img=img,
                                                        location=location,
                                                        type="bundle"))


class BaseEntity(object):
    
    def __init__(self, id_number, session=None):
        self.logger = logging.getLogger(__name__)

        self.id = str(id_number)

        if session:
            self.session = session
        else:
            s = Scraper()
            self.session = s.session


    def find_state(self):
        r = self.session.get(self.edit_url)
        self.edit_soup = bs(r.content, "lxml")

        
        self.state = copy.copy(self.edit_template)
        for k, v in self.state.items():
            if k == "dbDeliveryType":
                options = self._enum_radio_options(k, "ShipTo")
                if options:
                    try:
                        val = [o["value"] for o in options if o["selected"]][0]
                        self.state[k] = val
                    except IndexError:
                        self.state[k] = ""
                else:
                    self.state[k] = ""

            elif k == "dbContactMode":
                options = self._enum_radio_options(k, "tdContactMode")
                if options:
                    try:
                        val = [o["value"] for o in options if o["selected"]][0]
                    except IndexError:
                        val = ""
                    self.state[k] = val
                else:
                    self.state[k] = ""

            else: 
                x = self.edit_soup.find(id=k)

                if x.name.lower() == "input":
                    if x.attrs["type"].lower() == "checkbox":
                        try:
                            checked = x.attrs["checked"].lower()
                            if checked == "checked":
                                self.state[k] = "on"
                        except KeyError:
                            self.state[k] = ""
                    else: 
                        try:
                            self.state[k] = x.attrs['value']
                        except KeyError:
                            self.state[k] = ""
                elif x.name.lower() == "select":
                    options = self._enum_select_options(x)
                    for opt in options:
                        if opt["selected"]:
                            self.state[k] = opt["value"]
                elif x.name.lower() == "textarea":
                    self.state[k] = x.text
        

    def _enum_select_options(self, bs_select_obj):
        all_options = []
        raw_opts = bs_select_obj.find_all("option") 
        if raw_opts:
            for opt in raw_opts:
                d = {"value": "", "text": "", "selected": False}
                try:
                    if opt.attrs["selected"] == "selected":
                        d["selected"] = True
                except KeyError:
                    pass
                try:
                    d["value"] = opt.attrs["value"]
                except AttributeError:
                    d["value"] = ""

                d["text"] = opt.text
                all_options.append(d)

            return all_options
        else:
            all_options = None
    

    # radio buttons don't have the same id when posting, 
    # have to manually determine the right parent id to search through for inputs
    def _enum_radio_options(self, original_id, id_to_look_for):
        all_options = [] 
        elem = self.edit_soup.find(id=id_to_look_for)
        inputs = elem.find_all("input")
        for i in inputs:
            d = {"id": "", "value":"", "selected": False}
            try:
                checked = i.attrs["checked"]
                d["selected"] = True
            except KeyError:
                pass
    
            try:
                d["id"] = i.attrs["id"]
            except KeyError:
                pass
            
            try:
                d["value"] = i.attrs["value"]
            except KeyError:
                pass
                
            all_options.append(d)

        return all_options
        
    # modify property.setters to allow for a second boolean value to disable pushing changes.
    # in other words, just change state locally, don't push to SPS.
    # useful for batching changes instead of making a call per property.setter.
    def batch(self, value, value_type):
        
        push = True
        newvalue = ""

        if isinstance(value, tuple):
            if len(value) == 2:
                if isinstance(value[0], value_type):
                    if isinstance(value[1], bool):
                        push = value[1]
                        newvalue = value[0]
                    else:
                        raise Exception("second value in tuple needs to be of type '<class bool>'")
                elif not value[0]:
                    pass
                else:
                    raise Exception("value needs to be of type {}".format(value_type))
            else:
                raise Exception("tuple needs to have 2 items -- value and True/False for pushing changes")
        elif isinstance(value, value_type):
            push = True     # assume push is True if no push is provided
            newvalue = value
        else:
            raise Exception("value needs to be tuple or of type {}".format(value_type))

        return newvalue, push


    def change(self, values_dict): 
        self.form_data = copy.copy(self.state)
        for k, v in values_dict.items():
            self.form_data[k] = v

        p = self.session.post(self.post_url, data=self.form_data)
        self.find_state()
        return p


class Item(BaseEntity):

    def __init__(self, id, session=None):
        super().__init__(id_number=id, session=session)
        self.view_url = URLS.view_item.format(self.id)
        self.edit_url = URLS.edit_item.format(self.id)
        self.post_url = URLS.edit_item.format(self.id)
        self.quantity_url = URLS.quantity_item.format(self.id, "{0}") #int(time.time()) to be filled at call time
        
        self.edit_template = Config.SPS_ITEM_EDIT_TEMPLATE

        self.find_state()
        self.get_quantity() 

    def get_quantity(self):
        t = str(int(time.time()))
        r = self.session.get(self.quantity_url.format(t))
        self.quantity_soup = bs(r.content, "lxml")
        tables = self.quantity_soup.find_all("table")
        self.table = tables[0]
        cells = self.table.find_all("td")
        
        self.slabs = []
        for c in cells:
            try:
                inp = c.find("input")
                if inp.attrs["id"].startswith("Invrecord_"):
                    value = inp.attrs["value"]
                    j = json.loads(value)
                    j["image_management"] = URLS.detail_slab_picture.format(j["dbID"], j["dbIDOne"], j["dbIDTwo"])
                    self.slabs.append(j) 
            except AttributeError:
                pass
      
        #self.df = pd.DataFrame(self.slabs)


    @property
    def alternate_name(self):
        self._alternate_name = self.state["dbAlternateName"]
        return self._alternate_name
        
    
    @alternate_name.setter
    def alternate_name(self, value):
        value, push = self.batch(value, str)
        self.state["dbAlternateName"] = value
        if push:
            self.change(self.state)

    
    @property
    def thickness(self):
        
        select = self.edit_soup.find(id="dbThickness")
        self.thickness_options = self._enum_select_options(select)
        for opt in self.thickness_options:
            if opt["selected"]:
                self._thickness = {"name": opt["text"], "value": opt["value"]}
        return self._thickness


    @thickness.setter
    def thickness(self, value):
        
        #value is the human readable value -- e.g. 2cm, 3 cm etc.

        value, push = self.batch(value, str)
        value = value.replace(" ", "")
        select = self.edit_soup.find(id="dbThickness")
        self.thickness_options = self._enum_select_options(select)
        if value:
            for opt in self.thickness_options:
                if value == opt["text"].replace(" ", ""):
                    self.state["dbThickness"] = opt["value"]
        else:
            for opt in self.thickness_options:
                if not opt["value"]:
                    self.state["dbThickness"] = opt["value"]
        if push:
            self.change(self.state)



    @property
    def finish(self):
        select = self.edit_soup.find(id="dbFinish")
        self.finish_options = self._enum_select_options(select)
        for opt in self.finish_options:
            if opt["selected"]:
                self._finish = {"name": opt["text"], "value": opt["value"]}
        return self._finish


    @finish.setter
    def finish(self, value):
        value, push = self.batch(value, str)
        value = value.replace(" ", "")
        select = self.edit_soup.find(id="dbFinish")
        self.finish_options = self._enum_select_options(select)
        if value:
            for opt in self.finish_options:
                if value.lower() == opt["text"].replace(" ", "").lower():
                    self.state["dbFinish"] = opt["value"]
        else:
            for opt in self.finish_options:
                if not opt["value"]:
                    self.state["dbFinish"] = opt["value"]

        if push:
            self.change(self.state)


    @property
    def price_range(self):
        select = self.edit_soup.find(id="dbPriceRange")
        self.price_range_options = self._enum_select_options(select)
        for opt in self.price_range_options:
            if opt["selected"]:
                self._price_range = {"name":opt["text"], "value":opt["value"]}   
        return self._price_range


    @price_range.setter
    def price_range(self, value):
        value, push = self.batch(value, str)
        select = self.edit_soup.find(id="dbPriceRange")
        self.price_range_options = self._enum_select_options(select)
        if value:
            for opt in self.price_range_options:            
                if value.replace(" ", "").lower() == opt["text"].replace(" ", "").lower():
                    self.state["dbPriceRange"] = opt["value"]
        else:
            for opt in self.price_range_options:
                if not opt["value"]:
                    self.state["dbPriceRange"] = opt["value"]
        if push:
            self.change(self.state)



    @property
    def category(self):
        select = self.edit_soup.find(id="dbServiceCategoryID")
        self.category_options = self._enum_select_options(select)
        for opt in self.category_options:
            if opt["selected"]:
                self._category = {"name":opt["text"], "value":opt["value"]}   
        return self._category



    @category.setter
    def category(self, value):
        value, push = self.batch(value, str)
        select = self.edit_soup.find(id="dbServiceCategoryID")
        self.category_options = self._enum_select_options(select)
        if value:
            for opt in self.category_options:            
                if value.replace(" ", "").lower() == opt["text"].replace(" ", "").lower():
                    self.state["dbServiceCategoryID"] = opt["value"]
        else:
            for opt in self.category_options:
                if not opt["value"]:
                    self.state["dbServiceCategoryID"] = opt["value"]
        if push:
            self.change(self.state)
    

    @property
    def group(self):
        select = self.edit_soup.find(id="dbItemGroup")
        self.group_options = self._enum_select_options(select)
        for opt in self.group_options:
            if opt["selected"]:
                self._group = {"name":opt["text"], "value":opt["value"]}   
        return self._group


    @group.setter
    def group(self, value):
        value, push = self.batch(value, str)
        select = self.edit_soup.find(id="dbItemGroup")
        self.group_options = self._enum_select_options(select)
        if value:
            for opt in self.group_options:            
                if value.replace(" ", "").replace("-","").lower() == opt["text"].replace(" ", "").replace("-","").lower():
                    self.state["dbItemGroup"] = opt["value"]
        else:
            for opt in self.group_options:
                if not opt["value"]:
                    self.state["dbItemGroup"] = opt["value"]
        if push:
            self.change(self.state)


    @property
    def origin(self):
        select = self.edit_soup.find(id="dbOrigin")
        self.origin_options = self._enum_select_options(select)
        for opt in self.origin_options:
            if opt["selected"]:
                self._origin = {"name":opt["text"], "value":opt["value"]}   
        return self._origin


    @origin.setter
    def origin(self, value):
        value, push = self.batch(value, str)
        select = self.edit_soup.find(id="dbOrigin")
        self.origin_options = self._enum_select_options(select)
        if value:
            for opt in self.origin_options:            
                if value.replace(" ", "").lower() == opt["text"].replace(" ", "").lower():
                    self.state["dbOrigin"] = opt["value"]
        else:
            for opt in self.origin_options:
                if not opt["value"]:
                    self.state["dbOrigin"] = opt["value"]
        if push:
            self.change(self.state)


class Opportunity(BaseEntity):

    def __init__(self, id, session=None):
        super().__init__(id_number=id, session=session)
        self.view_url = URLS.view_opportunity.format(self.id)
        self.edit_url = URLS.edit_opportunity.format(self.id)
        self.post_url = URLS.post_opportunity.format(self.id, "{0}") #int(time.time()) to be filled at call time

        self.edit_template = Config.SPS_OPPORTUNITY_EDIT_TEMPLATE

        self.find_state()

    @property
    def number(self):
        self._number = self.state["dbTransactionNumber"]
        return self._number

    @number.setter
    def number(self, value):
        value, push = self.batch(value, str)
        self.state["dbTransactionNumber"] = value
        
        if push:
            self.change(self.state)
    
    @property
    def email(self):
        self._email = self.state["dbShipToEmail"]
        return self._email
    
    @email.setter
    def email(self, value):
        value, push = self.batch(value, str)
        self.state["dbShipToEmail"] = value

        if push:
            self.change(self.state)

    def get_datetime(self):
        rdt = self.state["dbTransactionDate"] + " " + self.state["dbTransactionTime"]
        dt = datetime.datetime.strptime(rdt, "%m/%d/%Y %I:%M %p")
        pdt = pendulum.instance(dt, tz="America/Chicago")
        return pdt
 
    @property
    def time(self):
        dt = self.get_datetime()
        self._time = dt.time()
        return self._time

    @time.setter
    def time(self, value):
        # value needs to be a datetime.time object with hours (24h format) & minutes
        value, push = self.batch(value, datetime.time)
        pdt = self.get_datetime()
        npdt = pdt.replace(hour=value.hour, minute=value.minute, second=value.second)
        self.state["dbTransactionTime"] = npdt.format("hh:mm A")
        
        if push:
            self.change(self.state)
         

    @property
    def date(self):
        rdt = self.state["dbTransactionDate"]
        nrdt = datetime.datetime.strptime(rdt, "%m/%d/%Y")
        self._date = pendulum.instance(nrdt, tz="America/Chicago")
        return self._date
    

    @date.setter
    def date(self, value):
        # value has to be a datetime value. It may include a time or not
        value, push = self.batch(value, datetime.datetime)
        pdv = pendulum.instance(value, tz="America/Chicago")
        self.state["dbTransactionDate"] = pdv.format("M/D/YYYY")

        if pdv.hour != 0:
            self.state["dbTransactionTime"] = pdv.format("hh:mm A")
             
        if push:
            self.change(self.state)


    @property
    def name(self):
        self._name = self.state["dbJobName"]
        return self._name
    
    @name.setter
    def name(self, value):
        value, push = self.batch(value, str)
        self.state["dbJobName"] = value
        if push:
            self.change(self.state)

    @property
    def phone(self):
        self._phone = self.state["dbShipToPhone1"]
        return self._phone
    
    @phone.setter
    def phone(self, value):
        value, push = self.batch(value, str)
        self.state["dbShipToPhone1"] = value
        
        if push:
            self.change(self.state)
        
   
    def create_quick_email_button(self):
        if self.email:
            # strip out old button/href
            n = self.name.split("<a")[0]

            # create new button

            button = n + " " + "<a href='mailto:{0}?subject=Checking In' target='_blank'><button>Quick Email</button></a>".format(self.email)
            if len(button) <= 150:
                self.state["dbJobName"] = button
                self.change(self.state)
            

    def create_email_button(self):
        template = """<a href="mailto:{email}?subject=Following%20Up&amp;body=Hi%20{fname}%2C%0A%0AThanks%20for%20visiting%20Stoneland!%0A%0AIt%20looks%20like%20you%20haven't%20decided%20on%20slabs%20yet.%0A%0APlease%20come%20and%20visit%20us%20again.%20We%20have%20many%20more%20varieties%20coming%20in%20all%20the%20time.%0A%0AWe're%20more%20than%20happy%20to%20assist%20you%20in%20upgrading%20your%20fabulous%20home!%0A%0AAre%20there%20any%20questions%20I%20can%20answer%20for%20you%20right%20now%3F%0A%0A%0ARegards%2C%0ASales%20Team%20%0AStoneland%20Inc.%0Awww.stonelandinc.com" target="_blank"><button>Follow Up</button></a>"""
        
        if self.email:
            self.state["dbInternalNotes"] = template.format(email=self.email, fname=self.name.split(" ")[0].strip())

        self.change(self.state)

