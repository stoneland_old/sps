#!/usr/bin/env python3

import os
import sys
import re
import errno
import logging
import logging.config
import urllib

from .urls import URLS


def update_url_params(url, new_params):
    # new_params need to be dict

    url_parts = list(urllib.parse.urlparse(url))            #break down the url into parts
    query  = dict(urllib.parse.parse_qsl(url_parts[4]))     #existing params to dict
    query.update(new_params)                                #update with new parameters
    url_parts[4] = urllib.parse.urlencode(query)            #encode params to url
    new_url = urllib.parse.urlunparse(url_parts)            #create new url with all params
    return new_url

# Clean name of stone as gathered from SPS
def clean_stone_name(raw_name):
    name = raw_name
    pat1 = re.compile('\d.*|t*\d.*|tt\s+\d*|aka:.*|:.*')
    pat2 = re.compile('\(.*\)')

    name = re.sub(pat1, '', name).strip()
    name = re.sub(pat2, '', name).strip()

    return name

        

"""
# Kill processes with specified name, given in a name
def kill_processes(process_names_list):
    
    for process in psutil.process_iter():
        pinfo = process.as_dict(attrs=['pid', 'name'])

        if len(process_names_list) == 1:
            if pinfo['name'].lower().startswith(process_names_list[0]):
                process.kill()
                print (process)
        else:
            for pn in process_names_list:
                if pinfo['name'].lower().startswith(pn):
                    process.kill()
                    print (process)
"""
