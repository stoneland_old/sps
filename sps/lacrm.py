#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from .config import Config
from .urls import URLS

import click
import requests

url = URLS.lacrm_api
user = Config.LACRM_USER_CODE
token = Config.LACRM_API_TOKEN

def search(search_term, search_type="both", slim_version=False):
    method_payload = {"UserCode": user,
                        "APIToken": token, 
                        "Function": "SearchContacts"}

    parameters = {}
    parameters["SearchTerms"] = search_term
    parameters["NumRows"] = "500"
    
    if search_type == "company":
        parameters["RecordType"] = "Companies"
    elif search_type == "contact":
        parameters["RecordType"] = "Contacts"
    
    method_payload["Parameters"] = json.dumps(parameters)
    
    r = requests.post(url, data=method_payload)

    if not slim_version:
        return r.json()["Result"]
    

def get(contact_id):
    method_payload = {"UserCode": user,
                      "APIToken": token,
                      "Function": "GetContact"}
    
    parameters = {}
    parameters["ContactId"] = contact_id
    parameters["NumRows"] = "500"
    method_payload["Parameters"] = json.dumps(parameters)

    r = requests.post(url, data=method_payload)
    
    return r.json()["Contact"]

def collect_company_emails(company_id):
    all_emails = []
    company = get(company_id)
    name = company["CompanyName"]
    
    for e in company["Email"]:
        all_emails.append(e["Text"])

    contacts = search(name, search_type="contact")
    for contact in contacts:
        if contact["CompanyId"] == company_id:
            for email in contact["Email"]:
                all_emails.append(email["Text"])
    
     
    return {"name":name, "id":company_id, "emails":all_emails}



