#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

# Standard library imports
import copy
import datetime
import html
import json
import logging
import os
import pytz
import random
import re
import requests
import shutil
import subprocess
import sys
import time
import urllib

# Third party imports
from bs4 import BeautifulSoup as bs
from deepdiff import DeepDiff
import feedparser
import git
import pandas as pd
import peewee

from oauth2client.service_account import ServiceAccountCredentials
from sparkpost import SparkPost

# Local Imports
from .config import Config
from .gsuite import MasterControlPlan
from .urls import URLS
from .tt import MSCContainer, AliancaContainer, HapagContainer, APLContainer, CMAContainer
from .outlook import Outlook
from . import utilities
from . import models


class Entity(object):
    
    def __init__(self, id, session=None):
        self.logger = logging.getLogger(__name__)

        self.id = str(po_id)
        
        self.view_url = ""
        self.edit_url = ""
        self.post_url = ""

        if session:
            self.session = session
        else:
            s = Scraper()
            self.session = s.session

        self.edit_template = {}

        r = self.session.get(self.view_url)
        self.soup = bs(r.content, "lxml")
        self.find_state()


    def find_state(self):
        r = self.session.get(self.edit_url)
        self.edit_soup = bs(r.content, "lxml")

        
        self.state = copy.copy(self.edit_template)
        for k, v in self.state.items():
            x = self.edit_soup.find(id=k)
            if x.name.lower() == "input":
                if x.attrs["type"].lower() == "checkbox":
                    try:
                        checked = x.attrs["checked"].lower()
                        if checked == "checked":
                            self.state[k] = "on"
                    except KeyError:
                        self.state[k] = ""
                else: 
                    try:
                        self.state[k] = x.attrs['value']
                    except KeyError:
                        self.state[k] = ""
            elif x.name.lower() == "select":
                option = x.find("option", {"selected":"selected"})
                try:
                    self.state[k] = option.attrs["value"].strip()
                except AttributeError:
                    print (option)
                    self.state[k] = ""
            elif x.name.lower() == "textarea":
                self.state[k] = x.text
    

    # modify property.setters to allow for a second boolean value to disable pushing changes.
    # in other words, just change state locally, don't push to SPS.
    # useful for batching changes instead of making a call per property.setter.
    def batch(self, value, value_type):
        
        push = ""
        newvalue = ""

        if isinstance(value, tuple):
            if len(value) == 2:
                if isinstance(value[0], value_type):
                    if isinstance(value[1], bool):
                        push = value[1]
                        newvalue = value[0]
                    else:
                        raise Exception("second value in tuple needs to be of type '<class bool>'")
                elif not value[0]:
                    pass
                else:
                    raise Exception("value needs to be of type {}".format(value_type))
            else:
                raise Exception("tuple needs to have 2 items -- value and True/False for pushing changes")
        elif isinstance(value, value_type):
            push = True     # assume push is True by default
            newvalue = value
        else:
            raise Exception("value needs to be tuple or of type {}".format(value_type))

        return newvalue, push


    def change(self, values_dict): 
        self.form_data = copy.copy(self.state)
        for k, v in values_dict.items():
            self.form_data[k] = v

        p = self.session.post(URLS.post_po.format(self.id, str(int(time.time()))), data=self.form_data)
        self.find_state()
        return p


  

