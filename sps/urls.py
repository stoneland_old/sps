#!/usr/bin/env python

# Local Imports
from .config import Config

class URLS(object):
    
    list_accounts = "https://{0}.stoneprofits.com/listAccounts.aspx".format(Config.URL_PREFIX)
    list_bills = "https://{0}.stoneprofits.com/listBills.aspx".format(Config.URL_PREFIX)
    list_customers = "https://{0}.stoneprofits.com/listCustomers.aspx?sort=CustomerName&order=ASC&page={1}".format(Config.URL_PREFIX, "{0}")
    list_events = "https://{0}.stoneprofits.com/listEvents.aspx".format(Config.URL_PREFIX)
    list_invoices = "https://{0}.stoneprofits.com/listInvoices.aspx".format(Config.URL_PREFIX)
    list_items = "https://{0}.stoneprofits.com/listItems.aspx?sort=ItemName&order=ASC&page={1}".format(Config.URL_PREFIX, "{0}")
    list_locations = "https://{0}.stoneprofits.com/listLocations.aspx".format(Config.URL_PREFIX)
    list_migrations = "https://{0}.stoneprofits.com/listMigrations.aspx".format(Config.URL_PREFIX)
    list_openholds = "https://{0}.stoneprofits.com/listOpportunities.aspx?list=ListHold&tab=5&sort=TransactionNumber&order=ASC".format(Config.URL_PREFIX)
    list_salesorders = "https://{0}.stoneprofits.com/listSaleOrders.aspx".format(Config.URL_PREFIX)
    list_salesorders = "https://{0}.stoneprofits.com/listSaleOrders.aspx?list=listSaleOrders&tab=0&sort=LSaleOrdersMIN.CreatedOn&order=DESC&page={1}".format(Config.URL_PREFIX, "{0}")
    list_purchases = "https://{0}.stoneprofits.com/listPurchases.aspx".format(Config.URL_PREFIX)
    list_returns = "https://{0}.stoneprofits.com/listReturnOrders.aspx".format(Config.URL_PREFIX)
    list_suppliers = "https://{0}.stoneprofits.com/listSuppliers.aspx".format(Config.URL_PREFIX)
    list_users = "https://{0}.stoneprofits.com/listUsers.aspx".format(Config.URL_PREFIX)
    list_vendors = "https://{0}.stoneprofits.com/listVendors.aspx".format(Config.URL_PREFIX)
    list_instock = "https://{0}.stoneprofits.com/R_S_InStock.aspx?q=ItemandLocation".format(Config.URL_PREFIX)
    list_onpo = "https://{0}.stoneprofits.com/R_S_ONPO.aspx".format(Config.URL_PREFIX)
    list_intransit = "https://{0}.stoneprofits.com/listInTransit.aspx?tab=0".format(Config.URL_PREFIX)
    list_onhold = "https://{0}.stoneprofits.com/R_S_OnHold.aspx?q=Item".format(Config.URL_PREFIX)
    list_inventoryhold = "https://{0}.stoneprofits.com/listInventoryHold.aspx?tab=0".format(Config.URL_PREFIX)
    list_inventorysearch = "https://{0}.stoneprofits.com/listInventorySearch.aspx?act=getItems".format(Config.URL_PREFIX)
    list_po = "https://{0}.stoneprofits.com/listPos.aspx?list=ListPurchaseOrders&tab=2&sort=TransDate&order=DESC&page={1}".format(Config.URL_PREFIX, "{0}")
    list_opportunities = "https://{0}.stoneprofits.com/listOpportunities.aspx?list=ListOpportunities&tab=0&sort=ID&order=DESC&page={1}".format(Config.URL_PREFIX, "{0}")
    view_opportunity = "https://{0}.stoneprofits.com/vOpportunity.aspx?ID={1}".format(Config.URL_PREFIX, "{0}")
    edit_opportunity = "https://{0}.stoneprofits.com/cOpportunity.aspx?tab=1&ID={1}".format(Config.URL_PREFIX, "{0}")
    post_opportunity = "https://{0}.stoneprofits.com/cOpportunity.aspx?act=Save&ID={1}&ActivityID=0&ActivityType=&add=&SubTransactionID=0&q={2}".format(Config.URL_PREFIX, "{0}", "{1}")
    
    report_purchase_analysis = "https://{0}.stoneprofits.com/R_PurchaseAnalysisReceivedvsSold.aspx?list=listReport&q1={1}&q2={2}&sort=ReceivedDate%2C+TransactionNumber%2C+ItemName%2C+SupplierName&order=ASC&page={3}".format(Config.URL_PREFIX, "{0}", "{1}", "{2}")
    view_customer = "https://{0}.stoneprofits.com/vCustomer.aspx?ID={1}#tab=0".format(Config.URL_PREFIX, "{0}")
    edit_customer = "https://{0}.stoneprofits.com/cCustomer.aspx?ID={1}&tab=1".format(Config.URL_PREFIX, "{0}")
    post_customer = "https://{0}.stoneprofits.com/cCustomer.aspx?act=Save&ID={1}&mirrorID=0&SalesRep3IDs=undefined".format(Config.URL_PREFIX, "{0}")

    detail_slab = "https://{0}.stoneprofits.com/vDetailInformation.aspx?ID={1}".format(Config.URL_PREFIX, "{0}")
    detail_slab_picture = "https://{0}.stoneprofits.com/fileupload/cUploadInventoryImages.aspx?Source=SIPL&SubSource=Slab&TransactionLineDetailID={1}&Lot={2}&Bundle={3}&q=cw".format(Config.URL_PREFIX, "{0}","{1}","{2}")

    detail_supplier = "https://{0}.stoneprofits.com/vSupplier.aspx?ID={1}#tab=0".format(Config.URL_PREFIX, "{0}")
    edit_supplier = "https://{0}.stoneprofits.com/cSupplier.aspx?ID={1}&tab=1".format(Config.URL_PREFIX, "{0}")

    search_lists = "https://{0}.stoneprofits.com/cSearchParty.aspx".format(Config.URL_PREFIX)
    
    detail_po = "https://{0}.stoneprofits.com/vPO.aspx?ID={1}".format(Config.URL_PREFIX, "{0}")
    edit_po = "https://{0}.stoneprofits.com/CPO.aspx?ID={1}".format(Config.URL_PREFIX, "{0}")
    post_po = "https://{0}.stoneprofits.com/cPO.aspx?act=Save&mode=&ID={1}&PrepurchaseID=&UpdateExchangeRate=false&q={2}&PurchaseIDs=&PrePaymentCount=0&PaymentandCreditMemoCount=0&PrevSupplierID=".format(Config.URL_PREFIX, "{0}", "{1}")
    close_po = "https://{0}.stoneprofits.com/vPO.aspx?status=close&ID=".format(Config.URL_PREFIX)
    open_po = "https://{0}.stoneprofits.com/vPO.aspx?status=open&ID=".format(Config.URL_PREFIX)

    view_item = "https://{0}.stoneprofits.com/vItem.aspx?ID={1}#tab=0".format(Config.URL_PREFIX, "{0}")
    edit_item = "https://{0}.stoneprofits.com/cItem.aspx?tab=1&ID={1}".format(Config.URL_PREFIX, "{0}")
    quantity_item = "https://{0}.stoneprofits.com/ListInventorySearch.aspx?act=getItems&ItemID={1}&q={2}".format(Config.URL_PREFIX, "{0}", "{1}")

    portal_admin = "https://{0}.stoneprofits.com/vAdminTool.aspx".format(Config.URL_PREFIX)
    portal_banking = "https://{0}.stoneprofits.com/vBankingHome.aspx".format(Config.URL_PREFIX)
    portal_home = "https://{0}.stoneprofits.com/vHome.aspx".format(Config.URL_PREFIX)
    portal_inventory = "https://{0}.stoneprofits.com/vInventoryHome.aspx".format(Config.URL_PREFIX)
    portal_ledger = "https://{0}.stoneprofits.com/vJournalHome.aspx".format(Config.URL_PREFIX)
    portal_payables = "https://{0}.stoneprofits.com/vPayable.aspx".format(Config.URL_PREFIX)
    portal_presales = "https://{0}.stoneprofits.com/vPreSalesHome.aspx".format(Config.URL_PREFIX)
    portal_purchasing = "https://{0}.stoneprofits.com/vPurchaseHome.aspx".format(Config.URL_PREFIX)
    portal_receivables = "https://{0}.stoneprofits.com/vReceivable.aspx".format(Config.URL_PREFIX)
    portal_reports = "https://{0}.stoneprofits.com/vReports.aspx".format(Config.URL_PREFIX)
    portal_sales = "https://{0}.stoneprofits.com/vSalesHome.aspx".format(Config.URL_PREFIX)
    portal_scheduling = "https://{0}.stoneprofits.com/vScheduleHome.aspx".format(Config.URL_PREFIX)

    util_base = "https://{0}.stoneprofits.com/".format(Config.URL_PREFIX)
    util_raw = "{0}.stoneprofits.com".format(Config.URL_PREFIX)
    util_signin = "https://{0}.stoneprofits.com/default.aspx".format(Config.URL_PREFIX)
    util_changepassword = "https://{0}.stoneprofits.com/cChangePassword.aspx".format(Config.URL_PREFIX)
    util_logout = "https://{0}.stoneprofits.com/default.aspx?act=logout".format(Config.URL_PREFIX)
    util_options = "https://{0}.stoneprofits.com/vSystemOptions.aspx".format(Config.URL_PREFIX)
    util_quicksearch = "https://{0}.stoneprofits.com/QuickSearch.aspx".format(Config.URL_PREFIX)

    rtinv_all = "{0}defaultAjax.aspx?act=ajax&page=1&perpage=-1&searchType=1".format(Config.REALTIME_INVENTORY)
    rtinv_detail = "{0}defaultAjax.aspx?act=GetvPage&ItemID=".format(Config.REALTIME_INVENTORY)    # need to append item id
    rtinv_noimage = "{0}image/imgnotbig.png".format(Config.REALTIME_INVENTORY)
    lacrm_api = "https://api.lessannoyingcrm.com"
   
 
