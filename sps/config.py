#!/usr/bin/env python

import os

from .config_sps_post_data import *
from .config_md_files import *

from dotenv import load_dotenv
load_dotenv(".env")

basedir = os.path.abspath(os.path.dirname(__file__))
envfile = os.path.join(os.getcwd(), ".env")
load_dotenv(envfile) # needs a .env file in working dir


class Config(object):
    URL_PREFIX = os.getenv("URL_PREFIX")
    REALTIME_INVENTORY = os.getenv("REALTIME_INVENTORY")
    SPS_FULLNAME = os.getenv("SPS_FULLNAME")
    SPS_USERNAME = os.getenv("SPS_USERNAME")
    SPS_PASSWORD = os.getenv("SPS_PASSWORD")
    SPS_SCREENSHOT = os.getenv("SPS_SCREENSHOT")
    OUTLOOK_EMAIL = os.getenv("OUTLOOK_EMAIL")
    OUTLOOK_PASSWORD = os.getenv("OUTLOOK_PASSWORD")
    GIT_REMOTE = os.getenv("GIT_REMOTE")
    GIT_LOCAL = os.getenv("GIT_LOCAL")
    GIT_EMAIL = os.getenv("GIT_EMAIL")
    GSHT_MASTER_CONTROL_PLAN = os.getenv("GSHT_MASTER_CONTROL_PLAN")
    LACRM_API_TOKEN = os.getenv("LACRM_API_TOKEN")
    LACRM_USER_CODE = os.getenv("LACRM_USER_CODE")
    DB_NAME = os.getenv("DB_NAME")
    DB_TYPE = os.getenv("DB_TYPE")
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")
    DB_HOST = os.getenv("DB_HOST")
    DB_PORT = os.getenv("DB_PORT")
    MAIL_HOST_PASSWORD = os.getenv("MAIL_HOST_PASSWORD")
    MAIL_FROM = os.getenv("MAIL_FROM")
    USERAGENTS = ["Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1",
                "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A5370a Safari/604.1",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36",
                "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1"]
    GMAIL_EMAIL_FROM = os.getenv("GMAIL_EMAIL_FROM")
    SPS_CUSTOMER_EDIT_TEMPLATE = customer_edit_template
    SPS_ITEM_EDIT_TEMPLATE = item_edit_template
    SPS_PO_EDIT_TEMPLATE = po_edit_template
    SPS_SUPPLIER_EDIT_TEMPLATE = supplier_edit_template
    SPS_OPPORTUNITY_EDIT_TEMPLATE = opportunity_edit_template
    MD_INVENTORY_FILE = inventory_file 
    MD_ITEM_FILE = item_file 
    MD_BUNDLE_FILE = bundle_file 


