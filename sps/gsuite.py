#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

# Standard library imports
import base64
import datetime
import re

from email.mime.text import MIMEText

import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Local Imports
from .urls import URLS
from .config import Config


class MasterControlPlan(object):
    
    def __init__(self):
        
        self.key = Config.GSHT_MASTER_CONTROL_PLAN
        self.scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name('gspread_creds.json', self.scope)
        gc = gspread.authorize(self.credentials)
        wks = gc.open_by_key(self.key)

        self.sheet = wks.sheet1
        self.data = self.sheet.get_all_records()
        self.prep_pos_for_update()
        #self.prep_containers_for_tracking()
        
     
    def format_date(self, datestr):
        #date is in format "dd-Mon-YYYY" in spreadsheet
        try:
            d = datetime.datetime.strptime(datestr, "%d-%b-%Y")
        except ValueError:
            d = ""
        return d
         
    def get_po_sail(self, po_number):
        po = ""
        for d in self.data:
            if str(d["PO"]) == str(po_number):
                po = d
        return po


    def format_container_number(self, cn):
        number = cn.replace(" ","").replace("/","").strip()
        rec = re.compile('[A-Z]{4}\d{7}')
        
        if len(rec.findall(number)) == 1:
            return number
        else:
            return None

    def prep_pos_for_update(self):
        self.pos_to_update = []
        today = datetime.datetime.combine(datetime.date.today(), datetime.time())
        for d in self.data:
            try:
                eta = datetime.datetime.strptime(d["ETA Final"], "%d-%b-%Y")    
                age = today - eta
                not_to_old = age <= datetime.timedelta(6)
                if d["PO"] and (d["CONTAINER"] or d["VESSEL"]) and (eta > today or not_to_old):
                    self.pos_to_update.append(str(d["PO"]))
            except ValueError as ve:
                print (ve)

    
    def prep_containers_for_tracking(self, number_only=False):
        from .tt import HapagContainer, MSCContainer, APLContainer, AliancaContainer, CMAContainer
        from .api import PO
    
        today = datetime.datetime.combine(datetime.date.today(), datetime.time())
        self.containers_to_update = []
        self.hapag_containers = []
        self.msc_containers = []
        self.apl_containers = []
        self.alianca_containers = []
        self.cma_containers = []
        
        for d in self.data:
            try:
                eta = datetime.datetime.strptime(d["ETA Final"], "%d-%b-%Y")
                age = today - eta 
                not_to_old = age <= datetime.timedelta(6)
                cn = self.format_container_number(d["CONTAINER"])
                if cn and (eta > today or not_to_old):
                    
                    if "hapag" in d["CARRIER"].lower():
                        if number_only:
                            self.hapag_containers.append({"po": d["PO"], "container": cn})
                            self.containers_to_update.append({"po": d["PO"], "container": cn})
                        else:
                            sc = HapagContainer(cn)
                            self.hapag_containers.append({"po": d["PO"], "container":sc})
                            self.containers_to_update.append({"po": d["PO"], "container":sc})
                    
                    elif "msc" in d["CARRIER"].lower():
                        if number_only:
                            self.containers_to_update.append({"po": d["PO"], "container": cn})
                            self.msc_containers.append({"po": d["PO"], "container": cn})
                        else:
                            sc = MSCContainer(cn)
                            self.msc_containers.append({"po": d["PO"], "container":sc})
                            self.containers_to_update.append({"po": d["PO"], "container": sc})
                    elif "apl" in d["CARRIER"].lower():
                        if number_only:
                            self.containers_to_update.append({"po": d["PO"], "container": cn})
                            self.apl_containers.append({"po": d["PO"], "container": cn})
                        else:
                            sc = APLContainer(cn)
                            self.apl_containers.append({"po":d["PO"], "container": sc})
                            self.containers_to_update.append({"po": d["PO"], "container":sc})
                    elif "alian" in d["CARRIER"].lower():
                        if number_only:
                            self.containers_to_update.append({"po": d["PO"], "container": cn})
                            self.alianca_containers.append({"po": d["PO"], "container": cn})
                        else:
                            sc = AliancaContainer(cn)
                            self.alianca_containers.append({"po": d["PO"], "container": sc})
                            self.containers_to_update.append({"po": d["PO"], "container": sc})
                    
                    elif "cma" in d["CARRIER"].lower():
                        if number_only:
                            self.containers_to_update.append({"po": d["PO"], "container": cn})
                            self.cma_containers.append({"po": d["PO"], "container": cn})
                        else:
                            sc = CMAContainer(cn)
                            self.cma_containers.append({"po": d["PO"], "container": sc})
                            self.containers_to_update.append({"po": d["PO"], "container": sc})


            except ValueError as ve:
                print (ve)
        

"""
class Gmail(object):

    def __init__(self):
        self.email_from = Config.GMAIL_EMAIL_FROM
        self.scopes = ['https://mail.google.com/']
        self.saf = 'gmail_creds.json'
        credentials = service_account.Credentials.from_service_account_file(self.saf, scopes=self.scopes)
        self.delegated_credentials = credentials.with_subject(self.email_from)
        self.service = build('gmail', 'v1', credentials=self.delegated_credentials)
        self.user_id = "me"

    
    def create_message(self, to, subject, message_text):

        message = MIMEText(message_text)
        message['to'] = to
        message['from'] = self.email_from
        message['subject'] = subject
        return {'raw': base64.urlsafe_b64encode(message.as_string().encode()).decode()}
        

    def send_message(self, message):    
        try:
            message = (self.service.users().messages().send(userId=self.user_id, body=message).execute())
            #print("Message Id: {0}".format(message["id"]))
            return message
        except errors.HttpError as error:
            print("An error occurred: {0}".format(error))



    def list_messages_with_labels(self, label_ids=[]):
        try:
            response = self.service.users().messages().list(userId=self.user_id, labelIds=label_ids).execute()
            messages = []
            if "messages" in response:
                messages.extend(response["messages"])

            while "nextPageToken" in response:
                page_token = response["nextPageToken"]
                response = service.users().messages().list(userId=self.user_id, labelIds=label_ids,pageToken=page_token).execute()
                messages.extend(response['messages'])
            return messages

        except errors.HttpError as error:
            print ("An error occurred: {0}".format(error))

    def get_label_id(self, label_name):
        results = self.service.users().labels().list(userId='me').execute()
        labels = results.get('labels', [])

        lids = []
        for l in labels:
            if label_name.lower() in l['name'].lower():
                lids.append(l['id'])
        return lids


    def get_message(self, msg_id):
        
        try:
            message = self.service.users().messages().get(userId=self.user_id, id=msg_id).execute()
            #print ("Message snippet: {0}".format(message["snippet"]))
            body = message["payload"]["parts"][0]["body"]["data"]
            #text_message = base64.urlsafe_b64decode(body.encode("utf-8"))
            return message
        except errors.HttpError as error:
            print ("An error occurred: {0}".format(error))


    def get_thread(self, thread_id):
        try:
            thread = self.service.users().threads().get(userId=self.user_id, id=thread_id).execute()
            messages = thread['messages']
            #print ("thread id: {0} - number of messages in this thread: {1}".format(thread["id"], len(messages)))
            return thread
        except errors.HttpError as error:
            print ("An error occurred: {0}".format(error))


    def get_hapag_lloyd_updates(self):
        hl_emails = []
        lids = self.get_label_id("hapag lloyd tracking")
        hl = self.list_messages_with_labels(lids)
        for h in hl:
            hl_emails.append(self.get_thread(h["threadId"])) 
        return hl_emails

"""   



