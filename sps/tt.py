#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

"""
tt.py - track and trace module to track container.
"""

# Standard library imports
import datetime
import os
import re
import time

from xml.etree import cElementTree as ctree

# Third Party Imports
from bs4 import BeautifulSoup as bs
import copy
import cv2
import feedparser
import pandas as pd
import pendulum
from PIL import Image
import pytesseract
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Local Imports
from .places import Location


def validate_container_number(number, separated=False, crp=None):
    number = number.replace(" ","").replace("/","").strip()
    last_digit_ok = False
    valid_format = False

    if not crp:
        crp = re.compile("(\D{4})(\d{7})")

    found = crp.search(number)
    if found:
        check_against = number[-1]
        values = []

        prefix = found.group(1)
        values.append(LETTERS[prefix[0]] * 1)
        values.append(LETTERS[prefix[1]] * 2)
        values.append(LETTERS[prefix[2]] * 4)
        values.append(LETTERS[prefix[3]] * 8)


        num = found.group(2)
        values.append(int(num[0]) * 16)
        values.append(int(num[1]) * 32)
        values.append(int(num[2]) * 64)
        values.append(int(num[3]) * 128)
        values.append(int(num[4]) * 256)
        values.append(int(num[5]) * 512)

        total1 = sum(values)
        cd = total1 - (int(total1/11)*11)
        if cd > 9:
            cd = int(str(cd)[-1])       #take last digit if 10 or greater
        if int(cd) == int(check_against):
            last_digit_ok = True
        else:
            print ("incorrect container number")

    else:
        print ("incorrect container number")

    match = crp.findall(number)
    if len(match) == 1:
        valid_format = True
    else:
        print ("invalid container number format")

    if valid_format and last_digit_ok:
        if separated:
            return (found.group(1), found.group(2))
        else:
            return number
    else:
        return None

def create_df(updates_dict):
    updates = copy.deepcopy(updates_dict)
    for u in updates:
        u["date"] = u["date"].strftime("%Y-%m-%d %H:%M:%S %z")
    df = pd.DataFrame(updates)
    df["date"] = pd.to_datetime(df["date"])
    return df


LETTERS = {
    "A": 10, "B": 12, "C": 13, "D": 14, "E": 15, "F": 16, "G": 17, "H": 18, "I": 19, 
    "J": 20, "K": 21, "L": 23, "M": 24, "N": 25, "O": 26, "P": 27, "Q": 28, "R": 29, 
    "S": 30, "T": 31, "U": 32, "V": 34, "W": 35, "X": 36, "Y": 37, "Z": 38}



        

class ShippingContainer(object):

    def __init__(self, cn):

        self.number = validate_container_number(cn)
        self.last_updated = pendulum.now(tz=("US/Central"))
        timezone = self.last_updated.tz.tzname(self.last_updated)
        self.last_updated_string = self.last_updated.format("DD-MMM-YYYY hh:MM A") + " " + timezone
        self._update_template = {
            "location":"","vessel":"", "voyage":"", "movement": "", "mode": "","date":""}
    
    def __repr__(self):
        return "<ShippingContainer {0} - [{1} LINE]>".format(self.number, self.shipping_line)
    
    def handle_saints(self, city_name):
        cn = city_name.lower()
        
        if cn.startswith("st. ") :
            cn = cn.replace("st. ", "saint ")
        elif cn.startswith("st "):
            cn = cn.replace("st ", "saint ")
        
        return cn.title()
        
            

class MSCContainer(ShippingContainer):
    
    def __init__(self, container_number):
        super().__init__(cn=container_number)
        self.shipping_line = "MSC"
        
        
        self.url = "http://wcf.mscgva.ch/publicasmx/Tracking.asmx/GetRSSTrackingByContainerNumber?ContainerNumber={0}".format(self.number)
        self.tracking_url = "https://www.msc.com/track-a-shipment"
        self.xml = feedparser.parse(self.url)["entries"]
        
        self.last_updated = pendulum.now(tz=("US/Central"))
        timezone = self.last_updated.tz.tzname(self.last_updated)
        self.last_updated_string = self.last_updated.format("DD-MMM-YYYY hh:MM A") + " " + timezone
        
        self.updates = []
        self.full_update()
        
            
 
    def full_update(self):

        for x in self.xml:
            df = pd.read_html(str(x["summary"]))
            df = df[0].drop(1,1)
            data = dict(df.to_dict(orient="split")["data"])
            
            pb_date = pendulum.from_format(x["published"], "ddd, DD MMM YYYY HH:mm:ss ZZ")
            up_date = pendulum.from_format(x["updated"], "YYYY-MM-DD[T]HH:mm:ssZ")
            
            temp = copy.deepcopy(self._update_template)
            if pb_date == up_date:
                temp["date"] = up_date.in_tz("UTC")
            else:
                temp["date"] = max(up_date, pb_date)

            try:
                temp["movement"] = data["Description"]
            except KeyError:
                temp["movement"] = ""

            try:
                city, subdivision, country = list(map(str.strip, data["Location"].split(",")))
                loc = Location(city=city, subdivision=subdivision, country=country)
                temp["location"] = loc
            except KeyError:
                temp["location"] = "" 
            
            try:
                temp["vessel"] = data["Vessel"]
            except KeyError:
                temp["vessel"] = ""

            try:
                temp["voyage"] = data["Voyage"]
            except KeyError:
                temp["voyage"] = ""

            if temp["voyage"]:
                temp["mode"] = "VE"

            self.updates.append(temp)

        self.df = create_df(self.updates)



class HapagLloydContainer(ShippingContainer):
 
    def __init__(self, container_number):
        super().__init__(cn=container_number)
        self.shipping_line = "HAPAG LLOYD"
        
        separated = validate_container_number(number=self.number, separated=True)

        self.searchable_number = separated[0] + "  " + separated[1]

        self.url = "https://194.9.149.83/com/hlag/esb/services/mobile/MobileService"
        self.tracking_url = "https://www.hapag-lloyd.com/en/online-business/tracing/tracing-by-container.html?container={0}".format(self.number)
        
        self.updates = []
        self.get_updates()
        self.parse_updates()
        
 
    def get_updates(self):
        
        headers = {
            'Connection': 'Keep-Alive',
            'Content-Type': 'text/xml;charset=UTF-8',
            'Host': 'svc01.hlag.com',
            'SOAPAction': 'urn:traceByContainer',
            'User-Agent': '',
            }

        body = """
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:hls="http://esb.hlag.com/services/mobile/MobileService">
            <soapenv:Header/>
            <soapenv:Body>
                <hls:traceByContainerRequest>
                    <hls:language>en</hls:language>
                    <hls:deviceId>A;en_US;201905040328148420833643;</hls:deviceId>
                    <hls:appVersion>7.2.3</hls:appVersion>
                    <hls:frameworkVersion>a1.7.4</hls:frameworkVersion>
                    <hls:Container>
                        <hls:containerNumber>{0}</hls:containerNumber>
                    </hls:Container>
                </hls:traceByContainerRequest>
            </soapenv:Body>
            </soapenv:Envelope>""".format(self.searchable_number)

        r = requests.post(self.url, data=body, headers=headers, verify=False)
        self.root = ctree.fromstring(r.content)
  

    def parse_updates(self):
        if self.root:
            base = '{{http://esb.hlag.com/services/mobile/MobileService}}{0}' 
            for e in self.root.iter(tag=base.format("eGrpTracingData")):

                temp = copy.deepcopy(self._update_template)
                _date = "0000-00-00"
                _time = "00:00:00"
                
                for i in e.iter():
                   
                    if i.tag.endswith("businessLocode"):
                        temp["location"] = Location(locode=i.text)
                    elif i.tag.endswith("eLineVessel"):
                        temp["vessel"] = i[0].text
                    elif i.tag.endswith("eLineOperation"):
                        temp["movement"] = i[0].text
                    elif i.tag.endswith("plannedArrDate"):
                        _date = i.text
                    elif i.tag.endswith("plannedArrTime"):
                        _time = i.text.split(".")[0]
                    elif i.tag.endswith("eLineMot"):
                        temp["mode"] = i[0].text
                    elif i.tag.endswith("scheduleVoyageNo"):
                        temp["voyage"] = i.text

                try:
                    pt = _date + " " + _time
                    rd = pendulum.from_format(pt, "YYYY-MM-DD HH:mm:ss", tz="UTC")
                    temp["date"] = rd
                except ValueError as ve:
                    pass

                self.updates.append(temp)
        
        self.updates.reverse()
        self.df = create_df(self.updates)
     

class APLContainer(ShippingContainer):
    
    def __init__(self, container_number):
        super().__init__(cn=container_number)
        self.shipping_line = "APL"
        
        self.url = "https://www.apl.com/ebusiness/tracking/search?SearchBy=Container&Reference={0}&search=Search".format(self.number)
        self.tracking_url = "https://www.apl.com/ebusiness/tracking/search?SearchBy=Container&Reference={0}&search=Search".format(self.number)        
        
        self.updates = []
        self.df = pd.DataFrame()
        
        if self.number:
            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.5",
                "Connection": "keep-alive",
                "Host": "www.apl.com",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
            }
            
            r = requests.get(self.url, headers=headers)
            self.soup = bs(r.content, "lxml")
            table = self.soup.find("table")
            rlocs = self.soup.find_all("span", {"class":"o-trackingnomap--place"})
            self._raw_locs = [rl.text for rl in rlocs]

            df = pd.read_html(str(table))[0]
            df = df.drop(["Unnamed: 1"], 1)

            self.updates = df.to_dict(orient="records")
            self.updates.reverse()
            self.format_updates()

    def format_updates(self):
        updates = []
        for u in self.updates:
            temp = copy.deepcopy(self._update_template)
            for k,v in u.items():
                if k.lower() == "date":
                    temp_date  = v
                elif k.lower() == "moves":
                    temp["movement"] = v
                elif k.lower() == "vessel":
                    temp["vessel"] = v
                elif k.lower() == "location":
                    v = v.split("Accessible")[0].strip()
                    try:
                        full_loc = [l for l in self._raw_locs if v in l][0]
                    except IndexError:
                        full_loc = v 
                    city, state, country = self.parse_location(full_loc)
                    temp["location"] = Location(city=city, subdivision=state, country=country)
                elif k.lower() == "voyage":
                    temp["voyage"] = v
            if temp["voyage"]:
                temp["mode"] = "VE"
            try: 
                temp["date"] = pendulum.from_format(temp_date, "ddd DD MMM YYYY HH:mm", tz=temp["location"].timezone).in_tz("UTC")
            except Exception as e:
                print (e)
            updates.append(temp)
        self.updates = updates
        self.df = create_df(self.updates)

    def parse_location(self, raw_loc):
        city, state, country = None, None, None

        # it varies - 'COLOMBO', 'ST LOUIS, MO, US', 'LOUISVILLE, KY'

        pat_csc = re.compile("(.+),\s+(\D{2})\s*\((\D{2})\)")
        pat_cs = re.compile("(.+),\s+(\D{2})")
        pat_cc = re.compile("(.+)\s*\((\D{2})\)")
        
        
        if "," not in raw_loc:
            f = pat_cc.match(raw_loc)
            if not f:
                city = raw_loc
            else:
                city = f.group(1).strip()
                country = f.group(2).strip()
        else:
            f = pat_csc.match(raw_loc)
            if not f:
                f = pat_cs.match(raw_loc)
                if not f:
                   print ("unknown error - couldn't parse")
                else:
                    city = f.group(1).strip()
                    state = f.group(2).strip()
            else:
                city = f.group(1).strip()
                state = f.group(2).strip()
                country = f.group(3).strip()
        return city, state, country
                    

class CMACGMContainer(ShippingContainer):
    
    def __init__(self, container_number):
        super().__init__(cn=container_number)
        self.shipping_line = "CMA CGM"
        
        self.url = "https://www.cma-cgm.com/ebusiness/tracking/search?SearchBy=Container&Reference={0}&search=Search".format(self.number)
        self.tracking_url = "https://www.cma-cgm.com/ebusiness/tracking/search?SearchBy=Container&Reference={0}&search=Search".format(self.number)

        
        self.updates = []
        self.df = pd.DataFrame()
        
        if self.number:
            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.5",
                "Connection": "keep-alive",
                "Host": "www.cma-cgm.com",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
            }

            r = requests.get(self.url, headers=headers)
            self.soup = bs(r.content, "lxml")
            table = self.soup.find("table")
            rlocs = self.soup.find_all("span", {"class":"o-trackingnomap--place"})
            self._raw_locs = [rl.text for rl in rlocs]

            df = pd.read_html(str(table))[0]
            df = df.drop(["Unnamed: 1"], 1)

            self.updates = df.to_dict(orient="records")
            self.updates.reverse()
            self.format_updates()

    def format_updates(self):
        updates = []
        for u in self.updates:
            temp = copy.deepcopy(self._update_template)
            for k,v in u.items():
                if k.lower() == "date":
                    temp_date  = v
                elif k.lower() == "moves":
                    temp["movement"] = v
                elif k.lower() == "vessel":
                    temp["vessel"] = v
                elif k.lower() == "location":
                    v = v.split("Accessible")[0].strip()
                    try:
                        full_loc = [l for l in self._raw_locs if v in l][0]
                    except IndexError:
                        full_loc = v 
                    city, state, country = self.parse_location(full_loc)
                    temp["location"] = Location(city=city, subdivision=state, country=country)
                elif k.lower() == "voyage":
                    temp["voyage"] = v
            if temp["voyage"]:
                temp["mode"] = "VE"
            try: 
                temp["date"] = pendulum.from_format(temp_date, "ddd DD MMM YYYY HH:mm", tz=temp["location"].timezone).in_tz("UTC")
            except Exception as e:
                print (e)
            updates.append(temp)
        self.updates = updates
        self.df = create_df(self.updates)

    def parse_location(self, raw_loc):
        city, state, country = None, None, None

        # it varies - 'COLOMBO', 'ST LOUIS, MO, US', 'LOUISVILLE, KY'

        pat_csc = re.compile("(.+),\s+(\D{2})\s*\((\D{2})\)")
        pat_cs = re.compile("(.+),\s+(\D{2})")
        pat_cc = re.compile("(.+)\s*\((\D{2})\)")
        
        
        if "," not in raw_loc:
            f = pat_cc.match(raw_loc)
            if not f:
                city = raw_loc
            else:
                city = f.group(1).strip()
                country = f.group(2).strip()
        else:
            f = pat_csc.match(raw_loc)
            if not f:
                f = pat_cs.match(raw_loc)
                if not f:
                   print ("unknown error - couldn't parse")
                else:
                    city = f.group(1).strip()
                    state = f.group(2).strip()
            else:
                city = f.group(1).strip()
                state = f.group(2).strip()
                country = f.group(3).strip()
        return city, state, country
    
        

class AliancaContainer(ShippingContainer):
        
    def __init__(self, container_number):
        super().__init__(cn=container_number)
        self.shipping_line = "Alianca"
        
        
        self.url = "https://www.alianca.com.br/linerportal/pages/alianca/tnt.xhtml?lang=en"
        self.tracking_url = "https://www.alianca.com.br/linerportal/pages/alianca/tnt.xhtml?lang=en"
        self.post_url = "https://www.alianca.com.br/linerportal/pages/alianca/tnt.xhtml;jsessionid={0}"
        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.5",
            "Connection": "keep-alive",
            "Host": "www.alianca.com.br",
            "Referer": "https://www.alianca.com.br/alianca/en/alianca/ecommerce_alianca/track_trace_alianca/index.html",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
            }

        self.session = requests.session()
        self.session.headers.update(headers)
        r = self.session.get(self.url)
        self.soup = bs(r.content, "lxml")
        for cookie in self.session.cookies:
            if cookie.name == "JSESSIONID":
                self.post_url = self.post_url.format(cookie.value)
        
        if self.number:
            self.search_container() 

    def search_container(self): 
        _today = datetime.datetime.combine(datetime.date.today(), datetime.time())
        today = pendulum.instance(_today)
        begin = today.subtract(days=60).format("DD-MMM-YYYY")
        end = today.add(days=12).format("DD-MMM-YYYY")
        
        inputs = self.soup.find_all("input")
        
        for i in inputs:
            if i.attrs["name"] == "javax.faces.ViewState":
                jvs = i.attrs["value"]
        
     
        payload = {
            "javax.faces.partial.ajax": "true",
            "javax.faces.source": "j_idt6:searchForm:j_idt8:search-submit",
            "javax.faces.partial.execute" : "j_idt6:searchForm",
            "javax.faces.partial.render" : "j_idt6:searchForm",
            "j_idt6:searchForm:j_idt8:search-submit": "j_idt6:searchForm:j_idt8:search-submit",
            "j_idt6:searchForm": "j_idt6:searchForm",
            "j_idt6:searchForm:j_idt8:inputReferences": self.number,
            "j_idt6:searchForm:j_idt8:inputDateFrom_input" : begin,
            "j_idt6:searchForm:j_idt8:inputDateTo_input": end,
            "javax.faces.ViewState": jvs
        }
        
        self.r = self.session.post(self.post_url, data=payload)
        self.soup = bs(self.r.content, "lxml")
        self.parse_updates(self.soup)
        self.df = create_df(self.updates)
        
    
    def parse_updates(self, soup):
        self.updates = []
        twrap = soup.find("div", {"class": "ui-datatable-tablewrapper"})
        table = twrap.find("table")
        rows = table.find_all("tr") 
        for row in rows:
            cells = row.find_all("td")
            temp = copy.deepcopy(self._update_template)
            if cells:
                try:
                    temp["date"] = pendulum.from_format(cells[0].text, "DD-MMM-YYYY HH:mm")
                except ValueError:
                    temp["date"] = ""
 
                temp["location"] = self._handle_location(cells[1].text)
                temp["movement"] = cells[2].text
                try:
                    if len(cells[3].contents) == 2:
                        p1 = cells[3].contents[0].text.strip()
                        p2 = cells[3].contents[1].text.strip()
                        temp["vessel"] = p1
                        temp["voyage"] = p2
                        temp["mode"] = "VE"
                    else:
                        temp["vessel"] = cells[3].text
                        if "truck" in cells[3].text.lower():
                            temp["mode"] = "TR"
                        elif "railway" in cells[3].text.lower():
                            temp["mode"] = "RA"
                        
                except Exception as e:
                    print (e)

                self.updates.append(temp)

    def _handle_location(self, raw_loc):
        splitlocs = raw_loc.split(" ")
        if len(splitlocs[-1].strip()) == 5:
            loc = Location(locode=splitlocs[-1].strip())
        else:
            loc = ""
        return loc


        

class ShipmentLink(object):

    def __init__(self):
        self.home_url = "https://www.shipmentlink.com/"
        self.signin_url = "https://www.shipmentlink.com/tam1/jsp/TAM1_Login.jsp"
        self.captcha_url = "https://www.shipmentlink.com/servlet/TUF1_CaptchaUtils"
        self.post_url = "https://www.shipmentlink.com/servlet/TAM1_LoginController.do?action=TAM1_LoginController&lang=en"
        self.track_url = "https://www.shipmentlink.com/servlet/TDB1_CargoTracking.do"
        
        self.last_updated = pendulum.now(tz=("US/Central"))
        timezone = self.last_updated.tz.tzname(self.last_updated)
        self.last_updated_string = self.last_updated.format("DD-MMM-YYYY hh:MM A") + " " + timezone
        
        headers = {
            "Host": "www.shipmentlink.com",
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            "Accept-Encoding": "gzip, deflate, br",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "Cookie": ""
            }

        self.session = requests.session()
        self.session.headers.update(headers)
        self.r = self.session.get(self.home_url)
        for c in self.session.cookies:
            if c.name == "JSESSIONID":
                headers["Cookie"] = "JSESSIONID={}".format(c.value)

        self.session.headers.update(headers)
        self.soup = bs(self.r.content, "lxml")
        self.signed_in = False
        self.sign_in()
        self.r = self.session.get(self.track_url)


    def sign_in(self):
        while not self.is_signed_in(self.soup):
            self.sign_in_once()

    def sign_in_once(self):
        r = self.session.get(self.signin_url)
        self.soup = bs(r.content, "lxml")
        raw_sid = self.soup.find("input", {"name": "sID"})
        sid = raw_sid.attrs["value"]
        text = self.captcha()
        form_data = {
            "sID": sid,
            "id": "me@dkar.org",
            "password": "Grnite001",
            "captcha_input": text,
            "chkRmId": "on"
            }
        self.r = self.session.post(self.post_url, data=form_data)
        #r = self.session.get(self.home_url)
        self.soup = bs(self.r.content, "lxml")
        

    
    def is_signed_in(self, soup):

        divs = soup.find_all("div")
        for d in divs:
            if "Welcome" in d.text and "Kar" in d.text:
                self.signed_in = True
        return self.signed_in



    def captcha(self):
        r = self.session.get(self.captcha_url, stream=True)
        path = "captcha.jpg"
        temp = "temp.png"
        if r.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        im = cv2.imread(path)
        gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        gray = cv2.medianBlur(gray, 3)
        cv2.imwrite(temp, gray)
        image = Image.open(temp).convert("L")
        text = pytesseract.image_to_string(image, config='--psm 7')
        os.remove(path)
        os.remove(temp)
        return text.replace(" ", "")

    def track(self):
        pass




"""
def get_shipping_line(name):
    name = name.lower().replace(" ","").strip()

    if "alian" in name:
        return "ALIANCA"
    elif "apl" in name:
        return "PL"
    elif "cma" or "cgm" or "cmacgm" or "cma-cgm" in name:
        return "CMA CGM"
    elif "hapag" or "lloyd" in name:
        return "HAPAG LLOYD"
    elif "msc" in name:
        return "MSC"
    elif "apm" in name:
        return "APM-MAERSK"
    elif "cosco" in name:
        return "COSCO"
    elif "csav" in name:
        return "CSAV"
    elif "evergreen" in name:
        return "EVERGREEN"
    elif "hamburg" or "hamburgsud" in name:
        return "HAMBURG SUD"
    elif "hanjin" in name:
        return "HANJIN"
    elif "hmm" or "hyundai" in name:
        return "HMM"
    elif "kawasaki" in name:
        return "KAWASAKI"
    elif "mitsui" or "mol" in name:
        return "MOL"
    elif "nyk" or "nippon" or "yusen" in name:
        return "NYK"
    elif "ocean" or "network" or "express" or "one" in name:
        return "ONE"
    elif "oocl" or "orient" or "overseas" in name:
        return "OOCL"
    elif "pil" or "pacific" in name:
        return "PIL"
    elif "wanhai" or "wan" or "hai" in name:
        return "WAN HAI"
    elif "x-press" or "xpress" in name:
        return "X-PRESS"
    elif "yangming" or "yang" or "ming" in name:
        return "YANG MING"
    elif "zim" in name:
        return "ZIM"
    """
