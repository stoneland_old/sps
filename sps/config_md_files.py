inventory_file = """+++
title = "Real Time Inventory"
+++"""

item_file = """+++
modalname = "{modalname}"
img = "{image}"
origin = "{origin}"
description = "{description}"
stonetype = "{stonetype}"
title = "{title}"
color = "{color}"
thickness = "{thickness}"
finish = "{finish}"
type = "{type}"
instock = "{instock}"
+++"""


bundle_file = """+++
bundle = "{bundle}"
thickness = "{thickness}"
finish = "{finish}"
height = "{height}"
length = "{length}"
totalslabs = "{totalslabs}"
totalsqft = "{totalsqft}"
img = "{img}"
location = "{location}"
type = "{type}"
+++"""