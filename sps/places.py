#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

from .config import Config

import os
import re
import sqlite3

import requests
import pandas as pd
from timezonefinder import TimezoneFinder
import unidecode

## TEST DATA

# Hapag Lloyd - timezone given UTC
locode_test = ["BRPEC", "USNYC", "USSTL"]   #DONE

# MSC - timezone given
citystatecountry_test = ["HYDERABAD, TG, IN", "NHAVA SHEVA, MH, IN", "NEW YORK, NY, US", "EAST SAINT LOUIS, IL, US", "ST LOUIS, MO, US"] #DONE

# APL & CMACGM - times are local to the place
citycountry_test = ['NORFOLK, VA (US)', 'NHAVA SHEVA (IN)', 'HYDERABAD (IN)', 'JAIPUR (IN)', 'MUNDRA (IN)']

# Alianca & Hamburg SUD - times are local to the place
citylocode_test = ["St Louis USSTL", "Louisville USLUI", "Norfolk USORF", "Santos BRSSZ", "Vitoria BRVIX"]



# countries and states as per ISO 3166 - https://www.iso.org/obp/ui/#search/code/
# cities as per UN LOCODES - https://www.unece.org/cefact/locode/service/location


class Location(object):

    def __init__(self, **kwargs):
        db_string = os.path.join(os.path.dirname(os.path.abspath(__file__)), "app.db")
        
        connection = sqlite3.connect(db_string)
        self.cursor = connection.cursor()
        #locs_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data.cities.csv")
        #self._all = pd.read_csv(locs_file)
        #self._all.drop(["change", "remarks"], axis=1, inplace=True)
        #self._all.dropna(axis=0, how="all", inplace=True)

        self.city = None
        self.subdivision = None
        self.country = None
        self.locode = None
        self.lat = None
        self.lng = None
        self.timezone = None
        self.df = None

        allowed_kwargs = ["city", "subdivision", "country", "locode"]
        for k, v in kwargs.items():
            if k.lower() == "city":
                check_city = kwargs.get("city")
                check_subdivision = kwargs.get("subdivision")
                check_country = kwargs.get("country")
                
                if check_city and check_subdivision and check_country:
                    self.find_with_city_subdivision_country(check_city, check_subdivision, check_country) 
                elif check_city and not check_subdivision and check_country:
                    self.find_with_city_country(check_city, check_country)
                elif check_city and check_subdivision and not check_country:
                    self.find_with_city_subdivision(check_city, check_subdivision)   
                elif check_city and not check_subdivision and not check_country:
                    self.find_with_city(check_city)
                else:
                    raise ValueError("missing values")
            elif k.lower() == "locode":
                check_locode = kwargs.get("locode")
                self.find_with_locode(check_locode)

    
    def __repr__(self):
        return "{0}, {1} {2}".format(self.city, self.subdivision, self.country)
    
    def __eq__(self, other):
        if isinstance(other, Location):
            try:
                if (self.locode) == (other.locode):
                    return True
                else:
                    return False
            except AttributeError:
                return False
        else:
            return False

    def __ne__(self, other):
        if not isinstance(other, Location):
            return True
        else:
            try:
                if self.locode != other.locode:
                    return True
                else:
                    return False
            except AttributeError:
                return True

    def __hash__(self):
        return hash(self.locode)

    def handle_saint(self, city_name):
        cnl = city_name.lower()
        cns = cnl.split(" ")

        for i, n in enumerate(cns):
            if len(n) == 2 and n.startswith("st"):
                cns[i] = "saint"
            elif len(n) == 3 and n.startswith("st."):
                cns[i] = "saint"
        
        return " ".join(cns)


    def find_with_locode(self, raw_locode, use_db=True):
        
        locode_pat = re.compile("(\D{2})(\D{3})")

        found = locode_pat.search(raw_locode)
        if found:
            self.locode = raw_locode
            country = found.group(1).lower()
            city = found.group(2).lower()
            if use_db:
                search = (city, country, )
                self.cursor.execute("select * from locode where code = ? collate nocase \
                                    and country = ? collate nocase;", search)
                res = self.cursor.fetchall()
                self.narrow_down_db(res)
            else:
                res = self._all.loc[(self._all.country.str.lower() == country) & \
                    (self._all.location.str.lower() == city)]
                self.narrow_down(res)

 
    def find_with_city_subdivision_country(self, raw_city, raw_subdivision, raw_country):
        raw_city = self.handle_saint(raw_city)
        search = (raw_city, raw_subdivision, raw_country, )
        self.cursor.execute("select * from locode where city=? collate nocase \
                            and subdivision=? collate nocase and country=? collate nocase;", search)
        res = self.cursor.fetchall()
        if len(res) == 0:
            self.find_with_city_country(raw_city, raw_country) 
        else:
            self.narrow_down_db(res)
        """     
        else:
            res = self._all.loc[(self._all.name.str.lower()==raw_city.lower()) & \
                    (self._all.country.str.lower()==raw_country.lower())]
            if len(res.index) > 1:
                res = self._all.loc[(self._all.name.str.lower()==raw_city.lower()) & \
                        (self._all.subdivision.str.lower()==raw_subdivision.lower()) & \
                        (self._all.country.str.lower()==raw_country.lower())]
                   
            self.narrow_down(res)
        """
 
    def find_with_city_country(self, raw_city, raw_country):
        raw_city = self.handle_saint(raw_city)
        search = (raw_city, raw_country, )
        self.cursor.execute("select * from locode where city=? collate nocase \
                            and country=? collate nocase;", search)
        res = self.cursor.fetchall()
        self.narrow_down_db(res)
        """    
        else:
            res = self._all.loc[(self._all.name.str.lower()==raw_city.lower()) & \
                    (self._all.country.str.lower()==raw_country.lower())]
            self.narrow_down(res)
        """

    def find_with_city_subdivision(self, raw_city, raw_subdivision):
        raw_city = self.handle_saint(raw_city)
        search = (raw_city, raw_subdivision, )
        self.cursor.execute("select * from locode where city=? collate nocase \
                            and subdivision=? collate nocase;", search)
        res = self.cursor.fetchall()
        self.narrow_down_db(res)
        """     
        else:
            res = self._all.loc[(self._all.name.str.lower()==raw_city.lower()) & \
                    (self._all.subdivision.str.lower()==raw_subdivision.lower())]
            self.narrow_down(res)
        """

    def find_with_city(self, raw_city):
        raw_city = self.handle_saint(raw_city)
        search = (raw_city, )
        self.cursor.execute("select * from locode where city=? collate nocase;", search)
        res = self.cursor.fetchall()
        self.narrow_down_db(res) 
        """
        else:
            res = self._all.loc[self._all.name.str.lower() == raw_city.lower()] 
            self.narrow_down(res)
        """

    def narrow_down_db(self, res):
        if len(res) == 1:
            row = res[0]
            self.city = row[2]
            self.subdivision = row[3]
            self.country = row[0]
            coord_str = row[-1]
            self._parse_coordinates(coord_str)
            self.locode = row[0] + row[1]
        elif len(res) > 1:
            nres = [r for r in res if r[4].lower().startswith("a") and "1" in r[5]]
            if len(nres) == 1:
                row = nres[0]
                self.city = row[2]
                self.subdivision = row[3]
                self.country = row[0]
                coord_str = row[-1]
                self._parse_coordinates(coord_str)
                self.locode = row[0] + row[1] 
        elif len(res) == 0:
            print ("not found")
        else:
            print ("too many values")


    def narrow_down(self,df):
        if len(df.index) == 1:
            df = df.where((pd.notnull(df)), None)
            self.df = df
            self.city = df.name.iloc[0]
            self.subdivision = df.subdivision.iloc[0]
            self.country = df.country.iloc[0]
            coord_str = df.coordinates.iloc[0]
            self._parse_coordinates(coord_str)
            self.locode = df.country.iloc[0] + df.location.iloc[0]
        
        else:
            ndf = df.loc[(df.function.str.contains("1")) & (df.status.str.startswith("A"))]
            if len(ndf.index) == 1:
                ndf = ndf.where((pd.notnull(ndf)), None)
                self.df = ndf
                self.city = ndf.name.iloc[0]
                self.country = ndf.country.iloc[0]
                self.subdivision = ndf.subdivision.iloc[0]
                coord_str = ndf.coordinates.iloc[0]
                self._parse_coordinates(coord_str)
                self.locode = ndf.country.iloc[0] + ndf.location.iloc[0]
            elif len(ndf.index) > 1:
                print ("too many values")
            elif len(ndf.index) == 0:
                print ("no values found")


    def _parse_coordinates(self, coord_str):
        lat, lng = None, None
        try:
            values = coord_str.strip().split(" ")
            for v in values:
                if v.lower().endswith("s"):
                    v = v[:-1]
                    lat = float("-" + v[:2] + "." + v[-2:])
                elif v.lower().endswith("n"):
                    v = v[:-1]
                    lat = float(v[:2] + "." + v[-2:])
                elif v.lower().endswith("e"):
                    v = v[:-1]
                    lng = float(v[:3] + "." + v[-2:])
                elif v.lower().endswith("w"):
                    v = v[:-1]
                    lng = float("-" + v[:3] + "." + v[-2:])
            self.lat = lat
            self.lng = lng
            self._find_timezone(self.lat, self.lng)
        except AttributeError as ae:
            print (ae)
            pass
     
    def _find_timezone(self, lat, lng):
        tf = TimezoneFinder(in_memory=True)
        self.timezone = tf.timezone_at(lat=lat, lng=lng)



class Places(object):


    def __init__(self):
        self.__locs_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data.cities.csv")
    
        self.cities = pd.read_csv(self.__locs_file) 
        #self.cities.drop(["change", "remarks"], axis=1, inplace=True)
        self.cities.dropna(axis=0, how="all", inplace=True)    

   
    def find_city_by_name(self, name):
         
        return self.cities.loc[self.cities['name'] == name]


    """
    def _find_country_by_name(self, name, code_only=False):
        name = name.lower()
        result = self.countries.loc[self.countries.name.str.lower() == name]
        
        if code_only:
            return result.max()["code"]
        else:
            return result

 
    def _find_country_by_code(self, code, name_only=False):
        code = code.lower()

        result = self.countries.loc[self.countries.code.str.lower() == code]
        
        if name_only:
            return result.max().max()
        else:
            return result
            

    def find_country(self, name):
        name = name.lower()
        d = {"code": "", "full": ""}

        if len(name) == 2:
            res = self._find_country_by_code(name)
        else:
            res = self._find_country_by_name(name)
        
        if len(res) == 1:
            d["country_full"] = res.name.max().lower()
            d["country_code"] = res.code.max().lower()
        return d 
    

    def find_subdivision(self, sub_name, country_name):
        d = {"subdivision_full": "", "country_full": "", "subdivision_code": "", "country_code": ""}
        sn = sub_name.lower()
        cn = country_name.lower()
        
        country = self.find_country(cn)
        res = self.subdivisions.loc[(self.subdivisions.name.str.contains("^"+sn, case=False, na=False)) & (self.subdivisions.country.str.lower() == country["country_code"])]
        if len(res) == 1:
            d["subdivision_full"] = res.name.max().lower()
            d["subdivision_code"] = res.code.max().lower()
            d["country_full"] = country["country_full"]
            d["country_code"] = country["country_code"]
            return d
        else:
            print ("narrow your search, too many values returned")
            return res 

    
    def find_city(self, city, subdivision, country):
        city = city.lower()
        pass 


    def find(self, *args, **kwargs):
        city = ""
        subdivision = ""
        country = ""
        if kwargs is not None and len(kwargs) == 1:
            for k, v in kwargs.items():
                if k.lower() == "city":
                    city = v
                elif k.lower() == "subdivision" or k.lower() == "state":
                    test = self.cities.loc[self.cities.subdivision == subdivision]
                    if test.empty:
                        subdivision = v.upper()
                elif k.lower() == "country":
                    
                    country = v
        
        res = self.cities.loc[(self.cities['name'] == city) | (self.cities.subdivision == subdivision) | (self.cities.country == country)]

        return res


    """
