#!/usr/bin/env python3

import os
import sys
import errno
import logging
import logging.config

from dotenv import load_dotenv

from sps.api import Opportunity, Scraper, Inventory, NSItem, NSItemDetail, MDFile, Repo, Email, PO, Supplier, Item, Customer
from sps.urls import URLS
from sps.config import Config
from sps.tt import MSCContainer, HapagLloydContainer, APLContainer, CMACGMContainer, AliancaContainer
from sps.outlook import Outlook
from sps.gsuite import MasterControlPlan
from sps.places import Location
from sps import models
from sps import lacrm
import sps.utilities

####################
# Configure Logging
####################
 
# If applicable, delete the existing log file to generate a fresh log file during each execution
if os.path.isfile("sps.log"):
    os.remove("sps.log")

models.db.init("app.db", pragmas={'journal_mode':'wal', 'foreign_keys':1, 'cache_size': -1 * 64000})
models.drop()
models.create()

 
# Create the Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
 
# Create the Handler for logging data to a file
logger_handler = logging.FileHandler('sps.log')
logger_handler.setLevel(logging.DEBUG)
 
# Create a Formatter for formatting the log messages
logger_formatter = logging.Formatter('%(name)s - %(funcName)s - %(levelname)s - %(message)s')
 
# Add the Formatter to the Handler
logger_handler.setFormatter(logger_formatter)
 
# Add the Handler to the Logger
logger.addHandler(logger_handler)
logger.info('Completed configuring logger()!')


