#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

import base64
import datetime
import dateutil
import email
import email.mime.multipart
import imaplib
import pytz
import re
import smtplib

from .urls import URLS
from .config import Config
from .gsuite import MasterControlPlan


class Outlook():
    def __init__(self):
        mydate = datetime.datetime.now()-datetime.timedelta(1)
        self.today = mydate.strftime("%d-%b-%Y")
        self.imap_server = "imap-mail.outlook.com"
        self.imap_port = 993
        self.smtp_server = "smtp-mail.outlook.com"
        self.smtp_port = 587
        self.username = Config.OUTLOOK_EMAIL
        self.password = Config.OUTLOOK_PASSWORD
        self.login()
        self.imap.select('"hapag lloyd"')
    
    def login(self):
        while True:
            try:
                self.imap = imaplib.IMAP4_SSL(self.imap_server,self.imap_port)
                r, d = self.imap.login(self.username, self.password)
                assert r == 'OK', 'login failed'
                print(" > Sign as ", d)
            except:
                print(" > Sign In ...")
                continue
            # self.imap.logout()
            break
   
 
    def send_email(self, recipient, subject, message):
        headers = "\r\n".join([
            "from: " + self.username,
            "subject: " + subject,
            "to: " + recipient,
            "mime-version: 1.0",
            "content-type: text/html"
        ])
        content = headers + "\r\n\r\n" + message
        while True:
            try:
                self.smtp = smtplib.SMTP(self.smtp_server, self.smtp_port)
                self.smtp.ehlo()
                self.smtp.starttls()
                self.smtp.login(self.username, self.password)
                self.smtp.sendmail(self.username, recipient, content)
                print("   email sent")
            except:
                print("   Sending email...")
                continue
            break


    def get_unread_emails(self):
        unread = []
        r, d =self.imap.uid("search", None, "UNSEEN")
        if r == "OK":
            ids = d[0].split()
            for i in ids:
                e = self.get_email(i)    # this marks email as read
                unread.append(e)
        return unread
    

    def get_emails_in_folder(self, folder_name):
        self.imap.select('"{}"'.format(folder_name))
        result, data = self.imap.uid('search', None, "ALL")
        ids = data[0].split()
        all_emails = []
        for i in ids:
            all_emails.append(self.get_email(i))
        return all_emails

 
    def get_last_email_with_subject(self, subject, folder_name="hapag lloyd"):
        self.imap.select('"{}"'.format(folder_name))
        r, d = self.imap.uid("search", None, 'SUBJECT "{0}"'.format(subject))
        if r == "OK" and d[0]:
            email_id = d[0].split()[-1]
            if email_id:
                email = self.get_email(email_id)
                return email
        else:
            return None
        
    
    def get_email(self, email_id):
        r, d = self.imap.uid("fetch", email_id, "(RFC822)")
        if r == "OK":
            self.raw_email = d[0][1]
            email_message = email.message_from_bytes(self.raw_email)
            self.email_message = EmailMessage(email_message)
            return self.email_message


    def get_emails_with_subject(self, subject):
        emails = []
        r, d = self.imap.uid("search", None, 'SUBJECT "{0}"'.format(subject))
        if r == "OK":
            ids = d[0].split()
            for i in ids:
                em =self.get_email(i)
                emails.append(em)
        return emails
    
    def send_hl_emails(self):
        from .tt import HapagContainer 
        wks = MasterControlPlan()
        for c in wks.hapag_containers:
            hc = HapagContainer(c)
            self.send_email("getinfo@hlag.com", hc.track_number, "trace this")


    def close(self):
        self.imap.logout()


class EmailMessage(object):
    def __init__(self, raw_email):
        self.raw = raw_email
        self.get_body()
        self.get_date()
        self.get_to_address()
        self.get_from_address()
        self.get_from_name()
        self.get_subject()
    

    def get_date(self):
        d = self.raw["date"]
        date = dateutil.parser.parse(d)
        self.date = date
        #central = pytz.timezone("US/Central")
        #self.date = date.astimezone(central)
        

    def get_body(self):
        self.body = ""
        if self.raw.is_multipart():
            for payload in self.raw.get_payload():
                if payload.is_multipart():
                    #TODO
                    pass
                else:
                    self.body = payload.get_payload()
        else:
            self.body = self.raw.get_payload()

    def get_subject(self):
        self.subject = self.raw["subject"]

    def get_from_address(self):
        self.from_address = self.raw["from"].split("<")[-1].replace(">", "").lower()

    def get_from_name(self):
        self.from_name = self.raw["from"].split("<")[0].strip().lower()
    
    def get_to_address(self):
        self.to_address = self.raw["to"].lower().replace("<", "").replace(">", "")

