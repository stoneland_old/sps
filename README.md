# SPS - an (unofficial) API for stoneprofits.com

## Installation

### Ubuntu 16.04



1. Install needed **python development libraries**.
    * `sudo apt-get install -y make build-essential libssl-dev zlib1g-dev python3-dev python-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev xvfb`


1. Install **pyenv** to manage python versions.
    1. Using [pyenv-installer](https://github.com/pyenv/pyenv-installer), run `curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash` to install pyenv and pyenv-virtualenv.
    1. Install [pyenv-virtualenvwrapper](https://github.com/pyenv/pyenv-virtualenvwrapper) by `git clone https://github.com/pyenv/pyenv-virtualenvwrapper.git $(pyenv root)/plugins/pyenv-virtualenvwrapper`.
    1. Install python version 3.5.2 `pyenv install 3.5.2`.
    1. Switch to it `pyenv global 3.5.2`.
    1. Install virtualenv tools `pip install virtualenv virtualenvwrapper`.
    1. Add the following to your .bashrc file to ensure virtualenvs work with pyenv.
    
    ```
    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/projects

    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
    pyenv virtualenvwrapper
    ```
    
    1. Run `source ~/.bashrc` to update these changes in your shell.
    1. Now virtualenvwrapper should work with pyenv `mkproject funstuff`, `deactivate`, `workon funstuff`.
    

1. Install **SPS**.
    1. In your virtualenv `pip install git+https://gitlab.com/stoneland/sps.git`
        * Add the '-e' flag if you want to hack on it. This will keep the source around `pip install -e git+git@gitlab.com:stoneland/sps.git`
    1. Add a `app_config.yml` file to your project root. Template "app_config" provided in this repo.


